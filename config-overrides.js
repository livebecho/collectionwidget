const BundleAnalyzerPlugin =
  require("webpack-bundle-analyzer").BundleAnalyzerPlugin;

module.exports = function override(config, env) {
  config.output = {
    ...config.output, // copy all settings
    filename: "static/js/[name].js",
    chunkFilename: "static/js/[name].chunk.js",
    jsonpFunction: "collectionWidget"
  };
  config.optimization.splitChunks = {
    cacheGroups: {
      default: false
    }
  };
  //CSS Overrides
  config.plugins[5].options.filename = "static/css/[name].css";
  config.plugins[5].options.chunkFilename = "static/css/[name].css";
  config.plugins.push(new BundleAnalyzerPlugin());

  config.optimization.runtimeChunk = false;
  return config;
};
