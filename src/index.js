import "core-js/stable";
import "regenerator-runtime/runtime";
import React, { Suspense } from "react";
import ReactDOM from "react-dom";
import "performance-polyfill";
import * as Sentry from "@sentry/react";
import { Integrations } from "@sentry/tracing";
import CollectionRoute from "./CollectionRoute";

Sentry.init({
  dsn: process.env.REACT_APP_SENTRY_DSN,
  autoSessionTracking: true,
  integrations: [new Integrations.BrowserTracing()],
  tracesSampleRate: 1.0,
  release: process.env.REACT_APP_SENTRY_PROJECT_NAME
});

const mountWidget = async (props) => {
  if (document.getElementById(props.mountElement)) {
    ReactDOM.render(
      <React.StrictMode>
        <Suspense fallback={() => <div>Loading</div>}>
          <CollectionRoute
            {...props}
            APP_SERVER_URL={props.apiDomain}
            REACT_APP_ENVIRONMENT={process.env.REACT_APP_ENVIRONMENT}
            REACT_APP_GOOGLE_ANALYTIC_ID={
              process.env.REACT_APP_GOOGLE_ANALYTIC_ID
            }
          />
        </Suspense>
      </React.StrictMode>,
      document.getElementById(props.mountElement)
    );
  }
};

window._livebecho = {
  ...window._livebecho,
  mountWidget: (props) => {
    const link = document.createElement("link");
    link.setAttribute("rel", "stylesheet");
    link.setAttribute(
      "href",
      "https://fonts.googleapis.com/css2?family=Montserrat:wght@400;500;600;700;900&display=swap"
    );
    document.head.appendChild(link);
    mountWidget(props);
  }
};

window.devMount(process.env);
