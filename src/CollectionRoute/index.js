import React, { useEffect, useState } from "react";
import { getCollection } from "../components/feed/Services/feedAPI.service";
export default (props) => {
  const [organizationToken, setOrganizationToken] = useState(undefined);
  console.log(props);
  useEffect(() => {
    if (props.organizationToken) {
      setOrganizationToken(props.organizationToken);
      getCollection(
        props.APP_SERVER_URL,
        props.collectionID,
        props.organizationToken
      ).then((response) => {
        console.log(response);
      });
    }
  }, []);
  return <h1>Hi</h1>;
};
