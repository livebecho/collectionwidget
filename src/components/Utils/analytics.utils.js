import uniqid from "uniqid";
import dayjs from "dayjs";
import * as rdd from "react-device-detect";
var utc = require("dayjs/plugin/utc");
dayjs.extend(utc);

let isSessionInfoSharedWithClient = false;

const get_local_storage_status = () => {
  try {
    localStorage.setItem("test", "test");
    localStorage.removeItem("test");
  } catch (e) {
    return false;
  }
  return true;
};

const triggerAnalyticsData = async (obj) => {
  try {
    let { appServerUrl, message, setCurrentSessionId, widgetType } = obj;
    let payload = message;
    if (get_local_storage_status()) {
      const sessionDetails = JSON.parse(
        localStorage.getItem("LB_SESSION_DETAILS")
      );
      if (!sessionDetails) {
        localStorage.setItem(
          "LB_SESSION_DETAILS",
          JSON.stringify({
            [payload.feedId]: {
              [payload.variantId]: {
                session_id: payload.sessionId,
                last_interacted_at: new Date()
              }
            }
          })
        );
      } else if (sessionDetails && !sessionDetails[payload.feedId]) {
        localStorage.setItem(
          "LB_SESSION_DETAILS",
          JSON.stringify({
            ...sessionDetails,
            [payload.feedId]: {
              [payload.variantId]: {
                session_id: payload.sessionId,
                last_interacted_at: new Date()
              }
            }
          })
        );
      } else if (
        sessionDetails &&
        sessionDetails[payload.feedId] &&
        !sessionDetails[payload.feedId][payload.variantId]
      ) {
        localStorage.setItem(
          "LB_SESSION_DETAILS",
          JSON.stringify({
            ...sessionDetails,
            [payload.feedId]: {
              ...sessionDetails[payload.feedId],
              [payload.variantId]: {
                session_id: payload.sessionId,
                last_interacted_at: new Date()
              }
            }
          })
        );
      } else {
        const variantSessionDetails =
          sessionDetails[payload.feedId][payload.variantId];
        let date = new Date();
        let lastInteractedAt = Math.abs(
          date - new Date(variantSessionDetails.last_interacted_at)
        );
        var diffMins = Math.round(lastInteractedAt / 1000 / 60);
        if (diffMins > 29) {
          let sessionId = uniqid();
          payload.sessionId = sessionId;
          setCurrentSessionId(sessionId);
          isSessionInfoSharedWithClient = false;
          localStorage.setItem(
            "LB_SESSION_DETAILS",
            JSON.stringify({
              ...sessionDetails,
              [payload.feedId]: {
                ...sessionDetails[payload.feedId],
                [payload.variantId]: {
                  session_id: sessionId,
                  last_interacted_at: date
                }
              }
            })
          );
          if (
            payload.event === "FEED_UNLOADED" ||
            payload.event === "INDIVIDUAL_ASSET_UNLOADED" ||
            payload.event === "PROFILE_UNLOADED" ||
            payload.event === "SET_SESSION_DEVICE_INFO" ||
            payload.event === "SET_WIDGET_TYPE" ||
            payload.event === "SET_FEED_VARIANT" ||
            payload.event === "FEED_LOADED" ||
            payload.event === "PROFILE_LOADED" ||
            payload.event === "INDIVIDUAL_ASSET_LOADED"
          ) {
            return;
          }
          if (window?._livebecho?.currentPage === "FEED_PAGE") {
            let data = {
              event: "FEED_LOADED",
              createdAt: dayjs().utc().format(),
              sessionId: sessionId,
              lastEvent: window.LAST_EVENT,
              feedId: payload.feedId,
              organizationUserId: payload.organizationUserId,
              userName: payload.userName,
              authorization: payload.authorization,
              domain: window.location.origin,
              variantId: payload.variantId
            };
            sendAnalyticsData(data, appServerUrl);
            let sessionDeviceData = {
              event: "SET_SESSION_DEVICE_INFO",
              deviceType: rdd.deviceType,
              device: `${rdd.mobileVendor}:${rdd.mobileModel}`,
              browser: `${rdd.browserName}:${rdd.fullBrowserVersion}`,
              osName: rdd.osName?.toUpperCase(),
              osVersion: rdd.osVersion,
              createdAt: dayjs().utc().format(),
              sessionId: sessionId,
              feedId: payload.feedId,
              organizationUserId: payload.organizationUserId,
              userName: payload.userName,
              authorization: payload.authorization,
              domain: window.location.origin,
              variantId: payload.variantId
            };
            sendAnalyticsData(sessionDeviceData, appServerUrl);
            let widgetTypeData = {
              event: "SET_WIDGET_TYPE",
              feedId: payload.feedId,
              widgetType: widgetType,
              createdAt: dayjs().utc().format(),
              sessionId: sessionId,
              organizationUserId: payload.organizationUserId,
              userName: payload.userName,
              authorization: payload.authorization,
              domain: window.location.origin,
              variantId: payload.variantId
            };
            sendAnalyticsData(widgetTypeData, appServerUrl);
            let variantIdData = {
              event: "SET_FEED_VARIANT",
              variantId: payload.variantId,
              feedId: payload.feedId,
              createdAt: dayjs().utc().format(),
              sessionId: sessionId,
              organizationUserId: payload.organizationUserId,
              userName: payload.userName,
              authorization: payload.authorization,
              domain: window.location.origin
            };
            sendAnalyticsData(variantIdData, appServerUrl);
          } else if (window?._livebecho?.currentPage === "PROFILE_PAGE") {
            let data = {
              event: "PROFILE_LOADED",
              createdAt: dayjs().utc().format(),
              sessionId: sessionId,
              lastEvent: window.LAST_EVENT,
              feedId: payload.feedId,
              organizationUserId: payload.organizationUserId,
              userName: payload.userName,
              authorization: payload.authorization,
              domain: window.location.origin,
              variantId: payload.variantId,
              profileId: payload.profileId
            };
            sendAnalyticsData(data, appServerUrl);
            let sessionDeviceData = {
              event: "SET_SESSION_DEVICE_INFO",
              osVersion: rdd.osVersion,
              deviceType: rdd.deviceType,
              device: `${rdd.mobileVendor}:${rdd.mobileModel}`,
              browser: `${rdd.browserName}:${rdd.fullBrowserVersion}`,
              osName: rdd.osName?.toUpperCase(),
              sessionId: sessionId,
              profileId: payload.profileId,
              createdAt: dayjs().utc().format(),
              feedId: payload.feedId,
              organizationUserId: payload.organizationUserId,
              userName: payload.userName,
              authorization: payload.authorization,
              domain: window.location.origin,
              variantId: payload.variantId
            };
            sendAnalyticsData(sessionDeviceData, appServerUrl);
            let widgetTypeData = {
              event: "SET_WIDGET_TYPE",
              feedId: payload.feedId,
              widgetType: widgetType,
              profileId: payload.profileId,
              createdAt: dayjs().utc().format(),
              sessionId: sessionId,
              organizationUserId: payload.organizationUserId,
              userName: payload.userName,
              authorization: payload.authorization,
              domain: window.location.origin,
              variantId: payload.variantId
            };
            sendAnalyticsData(widgetTypeData, appServerUrl);
            let variantIdData = {
              event: "SET_FEED_VARIANT",
              variantId: payload.variantId,
              feedId: payload.feedId,
              profileId: payload.profileId,
              createdAt: dayjs().utc().format(),
              sessionId: sessionId,
              organizationUserId: payload.organizationUserId,
              userName: payload.userName,
              authorization: payload.authorization,
              domain: window.location.origin
            };
            sendAnalyticsData(variantIdData, appServerUrl);
          } else if (
            window?._livebecho?.currentPage === "INDIVIDUAL_ASSET_PAGE"
          ) {
            let data = {
              event: "INDIVIDUAL_ASSET_LOADED",
              createdAt: dayjs().utc().format(),
              sessionId: sessionId,
              lastEvent: window.LAST_EVENT,
              feedId: payload.feedId,
              organizationUserId: payload.organizationUserId,
              userName: payload.userName,
              authorization: payload.authorization,
              domain: window.location.origin,
              variantId: payload.variantId,
              assetId: payload.assetId
            };
            sendAnalyticsData(data, appServerUrl);
            let sessionDeviceData = {
              event: "SET_SESSION_DEVICE_INFO",
              osVersion: rdd.osVersion,
              deviceType: rdd.deviceType,
              device: `${rdd.mobileVendor}:${rdd.mobileModel}`,
              browser: `${rdd.browserName}:${rdd.fullBrowserVersion}`,
              osName: rdd.osName?.toUpperCase(),
              createdAt: dayjs().utc().format(),
              sessionId: sessionId,
              feedId: payload.feedId,
              organizationUserId: payload.organizationUserId,
              userName: payload.userName,
              authorization: payload.authorization,
              domain: window.location.origin,
              variantId: payload.variantId,
              assetId: payload.assetId
            };
            sendAnalyticsData(sessionDeviceData, appServerUrl);
            let widgetTypeData = {
              event: "SET_WIDGET_TYPE",
              feedId: payload.feedId,
              widgetType: widgetType,
              createdAt: dayjs().utc().format(),
              sessionId: sessionId,
              organizationUserId: payload.organizationUserId,
              userName: payload.userName,
              authorization: payload.authorization,
              domain: window.location.origin,
              variantId: payload.variantId,
              assetId: payload.assetId
            };
            sendAnalyticsData(widgetTypeData, appServerUrl);
            let variantIdData = {
              event: "SET_FEED_VARIANT",
              variantId: payload.variantId,
              feedId: payload.feedId,
              createdAt: dayjs().utc().format(),
              sessionId: sessionId,
              organizationUserId: payload.organizationUserId,
              userName: payload.userName,
              authorization: payload.authorization,
              domain: window.location.origin,
              assetId: payload.assetId
            };
            sendAnalyticsData(variantIdData, appServerUrl);
          }
        } else {
          payload.sessionId = variantSessionDetails.session_id;
          localStorage.setItem(
            "LB_SESSION_DETAILS",
            JSON.stringify({
              ...sessionDetails,
              [payload.feedId]: {
                ...sessionDetails[payload.feedId],
                [payload.variantId]: {
                  session_id: variantSessionDetails.session_id,
                  last_interacted_at: date
                }
              }
            })
          );
        }
      }
    }
    sendAnalyticsData(payload, appServerUrl);
  } catch (err) {
    console.log(err);
  }
  return;
};

const sendAnalyticsData = (payload, appServerUrl) => {
  try {
    if (!isSessionInfoSharedWithClient) {
      window.postMessage(
        {
          action: "LB_SESSION_DETAILS",
          sessionID: payload.sessionId
        },
        "*"
      );
      isSessionInfoSharedWithClient = true;
    }
    var blob = new Blob([JSON.stringify({ data: payload })], {
      type: "application/json"
    });
    navigator.sendBeacon(`${appServerUrl}/api/v1/kinesis-analytics`, blob);
  } catch (err) {
    console.log(err);
  }
};

const feedEvents = async (obj) => {
  try {
    let data = {
      event: "FEED_LOADED",
      createdAt: dayjs().utc().format(),
      sessionId: obj.sessionId,
      lastEvent: obj.lastEvent,
      feedId: obj.feedId,
      organizationUserId: obj.organizationUserId,
      userName: obj.userName,
      authorization: obj.authorization,
      domain: obj.domain,
      variantId: obj.variantId
    };
    await triggerAnalytics({
      message: data,
      appServerUrl: obj.appServerUrl,
      setCurrentSessionId: obj.setCurrentSessionId,
      widgetType: obj.widgetType
    });
    let sessionDeviceData = {
      event: "SET_SESSION_DEVICE_INFO",
      deviceType: obj.deviceType,
      device: obj.device,
      browser: obj.browser,
      osName: obj.osName,
      osVersion: obj.osVersion,
      createdAt: dayjs().utc().format(),
      sessionId: obj.sessionId,
      feedId: obj.feedId,
      organizationUserId: obj.organizationUserId,
      userName: obj.userName,
      authorization: obj.authorization,
      domain: obj.domain,
      variantId: obj.variantId
    };
    await triggerAnalytics({
      message: sessionDeviceData,
      appServerUrl: obj.appServerUrl,
      widgetType: obj.widgetType,
      setCurrentSessionId: obj.setCurrentSessionId
    });
    let widgetTypeData = {
      event: "SET_WIDGET_TYPE",
      feedId: obj.feedId,
      widgetType: obj.widgetType,
      createdAt: dayjs().utc().format(),
      sessionId: obj.sessionId,
      organizationUserId: obj.organizationUserId,
      userName: obj.userName,
      authorization: obj.authorization,
      domain: obj.domain,
      variantId: obj.variantId
    };
    await triggerAnalytics({
      message: widgetTypeData,
      appServerUrl: obj.appServerUrl,
      widgetType: obj.widgetType,
      setCurrentSessionId: obj.setCurrentSessionId
    });
    let variantIdData = {
      event: "SET_FEED_VARIANT",
      variantId: obj.variantId,
      feedId: obj.feedId,
      createdAt: dayjs().utc().format(),
      sessionId: obj.sessionId,
      organizationUserId: obj.organizationUserId,
      userName: obj.userName,
      authorization: obj.authorization,
      domain: obj.domain
    };
    await triggerAnalytics({
      message: variantIdData,
      appServerUrl: obj.appServerUrl,
      widgetType: obj.widgetType,
      setCurrentSessionId: obj.setCurrentSessionId
    });
  } catch (err) {
    console.log(err);
  }
};

const profileEvents = async (obj) => {
  try {
    let data = {
      event: "PROFILE_LOADED",
      createdAt: dayjs().utc().format(),
      sessionId: obj.sessionId,
      lastEvent: obj.lastEvent,
      feedId: obj.feedId,
      organizationUserId: obj.organizationUserId,
      userName: obj.userName,
      authorization: obj.authorization,
      domain: obj.domain,
      variantId: obj.variantId,
      profileId: obj.profileId
    };
    await triggerAnalytics({
      message: data,
      appServerUrl: obj.appServerUrl,
      widgetType: obj.widgetType,
      setCurrentSessionId: obj.setCurrentSessionId
    });
    let sessionDeviceData = {
      event: "SET_SESSION_DEVICE_INFO",
      deviceType: obj.deviceType,
      device: obj.device,
      browser: obj.browser,
      osName: obj.osName,
      profileId: obj.profileId,
      osVersion: obj.osVersion,
      createdAt: dayjs().utc().format(),
      sessionId: obj.sessionId,
      feedId: obj.feedId,
      organizationUserId: obj.organizationUserId,
      userName: obj.userName,
      authorization: obj.authorization,
      domain: obj.domain,
      variantId: obj.variantId
    };
    await triggerAnalytics({
      message: sessionDeviceData,
      appServerUrl: obj.appServerUrl,
      widgetType: obj.widgetType,
      setCurrentSessionId: obj.setCurrentSessionId
    });
    let widgetTypeData = {
      event: "SET_WIDGET_TYPE",
      feedId: obj.feedId,
      widgetType: obj.widgetType,
      profileId: obj.profileId,
      createdAt: dayjs().utc().format(),
      sessionId: obj.sessionId,
      organizationUserId: obj.organizationUserId,
      userName: obj.userName,
      authorization: obj.authorization,
      domain: obj.domain,
      variantId: obj.variantId
    };
    await triggerAnalytics({
      message: widgetTypeData,
      appServerUrl: obj.appServerUrl,
      widgetType: obj.widgetType,
      setCurrentSessionId: obj.setCurrentSessionId
    });
    let variantIdData = {
      event: "SET_FEED_VARIANT",
      variantId: obj.variantId,
      feedId: obj.feedId,
      profileId: obj.profileId,
      createdAt: dayjs().utc().format(),
      sessionId: obj.sessionId,
      organizationUserId: obj.organizationUserId,
      userName: obj.userName,
      authorization: obj.authorization,
      domain: obj.domain
    };
    await triggerAnalytics({
      message: variantIdData,
      appServerUrl: obj.appServerUrl,
      widgetType: obj.widgetType,
      setCurrentSessionId: obj.setCurrentSessionId
    });
  } catch (err) {
    console.log(err);
  }
};

const individualAssetEvents = async (obj) => {
  try {
    let data = {
      event: "INDIVIDUAL_ASSET_LOADED",
      createdAt: dayjs().utc().format(),
      sessionId: obj.sessionId,
      lastEvent: obj.lastEvent,
      feedId: obj.feedId,
      organizationUserId: obj.organizationUserId,
      userName: obj.userName,
      authorization: obj.authorization,
      domain: obj.domain,
      variantId: obj.variantId,
      assetId: obj.assetId
    };
    await triggerAnalytics({
      message: data,
      appServerUrl: obj.appServerUrl,
      widgetType: obj.widgetType,
      setCurrentSessionId: obj.setCurrentSessionId
    });
    let sessionDeviceData = {
      event: "SET_SESSION_DEVICE_INFO",
      deviceType: obj.deviceType,
      device: obj.device,
      browser: obj.browser,
      osName: obj.osName,
      osVersion: obj.osVersion,
      createdAt: dayjs().utc().format(),
      sessionId: obj.sessionId,
      feedId: obj.feedId,
      organizationUserId: obj.organizationUserId,
      userName: obj.userName,
      authorization: obj.authorization,
      domain: obj.domain,
      variantId: obj.variantId,
      assetId: obj.assetId
    };
    await triggerAnalytics({
      message: sessionDeviceData,
      appServerUrl: obj.appServerUrl,
      widgetType: obj.widgetType,
      setCurrentSessionId: obj.setCurrentSessionId
    });
    let widgetTypeData = {
      event: "SET_WIDGET_TYPE",
      feedId: obj.feedId,
      widgetType: obj.widgetType,
      createdAt: dayjs().utc().format(),
      sessionId: obj.sessionId,
      organizationUserId: obj.organizationUserId,
      userName: obj.userName,
      authorization: obj.authorization,
      domain: obj.domain,
      variantId: obj.variantId,
      assetId: obj.assetId
    };
    await triggerAnalytics({
      message: widgetTypeData,
      appServerUrl: obj.appServerUrl,
      widgetType: obj.widgetType,
      setCurrentSessionId: obj.setCurrentSessionId
    });
    let variantIdData = {
      event: "SET_FEED_VARIANT",
      variantId: obj.variantId,
      feedId: obj.feedId,
      createdAt: dayjs().utc().format(),
      sessionId: obj.sessionId,
      organizationUserId: obj.organizationUserId,
      userName: obj.userName,
      authorization: obj.authorization,
      domain: obj.domain,
      assetId: obj.assetId
    };
    await triggerAnalytics({
      message: variantIdData,
      widgetType: obj.widgetType,
      appServerUrl: obj.appServerUrl,
      setCurrentSessionId: obj.setCurrentSessionId
    });
  } catch (err) {
    console.log(err);
  }
};

export const triggerProfileEvents = (obj) => profileEvents(obj);

export const triggerFeedEvents = (obj) => feedEvents(obj);

export const triggerIndividualAssetEvents = (obj) => individualAssetEvents(obj);

export const triggerAnalytics = (obj) => triggerAnalyticsData(obj);
