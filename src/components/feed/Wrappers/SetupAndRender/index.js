import React from "react";
import Routes from "../../Routes";

export default (props) => {
  return (
    <>
      {props.currentPage === "FEED_PAGE" && <Routes.FeedPage {...props} />}
      {props.currentPage === "PROFILE_PAGE" && (
        <Routes.UserProfile {...props} />
      )}
      {props.currentPage === "INDIVIDUAL_ASSET_PAGE" && (
        <Routes.IndividualAsset {...props} />
      )}
    </>
  );
};
