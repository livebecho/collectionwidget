import axios from "axios";
import React, { useContext, useEffect, useState } from "react";
import NotificationContextProvider, {
  NotificationContext
} from "../../Context/Notification.context";
import CircularLoader from "../../Components/Loader";

const InterceptionManager = (props) => {
  const notification = useContext(NotificationContext);
  const [isLoading, setIsLoading] = useState(null);
  const [loaderType, setLoaderType] = useState(undefined);

  useEffect(() => {
    axios.defaults.baseURL = props.APP_SERVER_URL;
    axios.interceptors.request.use((req) => {
      req.headers.Accept = "application/json";
      req.headers.common["Content-Type"] = "application/json";
      if (req.params?.globalLoader) {
        setIsLoading(true);
        setLoaderType("circular");
      }
      if (window.self === window.top) {
        setLoaderType("linear");
        setIsLoading(true);
      }
      return req;
    });

    axios.interceptors.response.use(
      (res) => {
        try {
          if (res.data.message) {
            if (res.data.success) {
              notification.setNotificationObject({
                type: res?.config?.headers?.successType || "success",
                message: res.data.message
              });
            } else {
              notification.setNotificationObject({
                type: "error",
                message: res.data.message
              });
            }
          }
          setIsLoading(false);
          return res;
        } catch (err) {
          console.log(err);
        }
      },
      (error) => {
        console.log(error);
      }
    );
  }, []);

  return (
    <>
      {isLoading && <>{loaderType == "linear" ? null : <CircularLoader />}</>}
    </>
  );
};

export default (props) => {
  return (
    <NotificationContextProvider>
      <InterceptionManager {...props} />
    </NotificationContextProvider>
  );
};
