import React, { createContext, useState, useRef } from "react";
import {
  getPublicFeed,
  getFeedByProfile,
  getFeedByAsset
} from "../Services/feedAPI.service.js";
import {
  triggerAnalytics,
  triggerFeedEvents,
  triggerProfileEvents,
  triggerIndividualAssetEvents
} from "../../Utils/analytics.utils";
import * as rdd from "react-device-detect";
import dayjs from "dayjs";
import addAssetIdToLocalStorage from "../Services/addAssetIdToLocalStorage.service.js";
var utc = require("dayjs/plugin/utc");
dayjs.extend(utc);

export const StoriesContext = createContext();

const getSeenAssetIds = (feedID, variantID) => {
  const lbLastSeenPostIds = JSON.parse(
    localStorage.getItem("LB_LAST_SEEN_POST_IDS")
  );
  if (
    !lbLastSeenPostIds ||
    !lbLastSeenPostIds[feedID] ||
    !lbLastSeenPostIds[feedID][variantID]
  ) {
    return [];
  } else {
    return lbLastSeenPostIds[feedID][variantID];
  }
};
export default (props) => {
  const [showProfile, setShowProfile] = useState(true);
  const [carouselContainerWidth, setCarouselContainerWidth] =
    useState(undefined);

  const [filterResetApiInProgress, setFilterResetApiInProgress] =
    useState(false);
  const itemsPushedDownBecauseOfFreshness = useRef([]);
  const freshnessEnabled = true;
  const noOfItemsBeforeNextAPI = 3;
  const [feedTheme, setFeedTheme] = useState({});
  const [hasUserChangedSlide, setHasUserChangedSlide] = useState(false);
  const [containerFeedWidth, setContainerFeedWidth] = useState(undefined);
  const [nativeTapTriggered, setNativeTapTriggered] = useState(false);
  const [organizationDetails, setOrganizationDetails] = useState({
    userID: "",
    userName: ""
  });
  const [currentActiveItemId, setCurrentActiveItemId] = useState(undefined);
  const [currentSessionId, setCurrentSessionId] = useState(undefined);
  const [storiesProps, setStoriesProps] = useState({});
  const [hasMore, setHasMore] = useState(true);
  const viewedAssetsRef = useRef([]);
  const itemsPerPage = 10;
  const pageNumberRef = useRef(1);
  const lastPageRequest = useRef(0);
  const [feed, setFeed] = useState({
    items: [],
    _id: "",
    name: "",
    status: "",
    description: "",
    photoURL: "",
    coverImage: "",
    photoURLFallback: "",
    coverImageFallback: ""
  });

  const [profileMeta, setProfileMeta] = useState(undefined);
  const currentUserId = useRef();
  currentUserId.current = organizationDetails.userID;

  const currentUserName = useRef();
  currentUserName.current = organizationDetails.userName;
  const productId = useRef(props.productID);

  const registerBeforeUnload = (feedId) => {
    document.addEventListener("visibilitychange", function logData() {
      if (document.visibilityState === "hidden") {
        if (window?._livebecho?.currentPage === "FEED_PAGE") {
          postFeedUnLoaded("FEED_UNLOADED");
        } else if (window?._livebecho?.currentPage === "PROFILE_PAGE") {
          postFeedUnLoaded("PROFILE_UNLOADED");
        } else if (
          window?._livebecho?.currentPage === "INDIVIDUAL_ASSET_PAGE"
        ) {
          postFeedUnLoaded("INDIVIDUAL_ASSET_UNLOADED", feedId);
        }
      } else if (document.visibilityState === "visible") {
        if (window?._livebecho?.currentPage === "FEED_PAGE") {
          postFeedLoaded("FEED_LOADED");
        } else if (window?._livebecho?.currentPage === "PROFILE_PAGE") {
          postFeedLoaded("PROFILE_LOADED");
        } else if (
          window?._livebecho?.currentPage === "INDIVIDUAL_ASSET_PAGE"
        ) {
          postFeedLoaded("INDIVIDUAL_ASSET_LOADED", feedId);
        }
      }
    });
  };

  const postFeedUnLoaded = (event, feedId) => {
    if (window.LAST_EVENT !== event) {
      postAnalyticsAPI(event, {}, feedId);
    }
    window.LAST_EVENT = event;
  };

  const postFeedLoaded = (event, feedID) => {
    if (window.UNLOADED || window.LAST_EVENT === event) {
      return;
    }
    postAnalyticsAPI(event, {}, feedID);
    window.LAST_EVENT = event;
  };
  const postAnalyticsAPI = (event, extraParametersObject, feedId) => {
    const message = {
      event: event,
      feedId: storiesProps.feedID || feedId,
      assetId: storiesProps.assetID,
      createdAt: dayjs().utc().format(),
      sessionId: currentSessionId,
      lastEvent: window.LAST_EVENT,
      organizationUserId: currentUserId.current,
      userName: currentUserName.current,
      authorization: storiesProps.organizationToken,
      domain: window.location.origin,
      variantId: storiesProps.variantID || "DEFAULT_FEED",
      ...extraParametersObject
    };
    triggerAnalytics({
      appServerUrl: storiesProps.APP_SERVER_URL,
      message: message,
      setCurrentSessionId: setCurrentSessionId,
      widgetType: "STORIES"
    });
  };

  const isFreshnessLogicEnabled = () => {
    return (
      freshnessEnabled && props.currentPage === "FEED_PAGE" && !props.productID
    );
  };

  const markAssetAsViewed = (event, extraParametersObject) => {
    if (isFreshnessLogicEnabled()) {
      addAssetIdToLocalStorage(
        extraParametersObject.assetId,
        storiesProps.feedID,
        storiesProps.variantID || "DEFAULT_FEED"
      );
    }
    if (viewedAssetsRef.current.indexOf(extraParametersObject.itemId) === -1) {
      viewedAssetsRef.current = viewedAssetsRef.current.concat(
        extraParametersObject.itemId
      );
      postAnalyticsAPI(event, extraParametersObject);
    }
  };

  const getFeed = async (obj) => {
    let itemsToShow = obj?.itemsToShow || [];
    if (obj?.isFeedRefreshed && !obj.dontUpdatePageNo) {
      pageNumberRef.current = 1;
    }
    if (pageNumberRef.current === 1) {
      setCurrentActiveItemId(undefined);
      itemsPushedDownBecauseOfFreshness.current = [];
    }
    if (pageNumberRef.current === lastPageRequest.current) {
      return;
    }
    lastPageRequest.current = pageNumberRef.current;
    let res = await getPublicFeed(
      storiesProps.APP_SERVER_URL,
      storiesProps.feedID,
      storiesProps.organizationToken,
      pageNumberRef.current,
      itemsPerPage,
      false,
      storiesProps.filter,
      storiesProps.assetID,
      productId.current
    );
    setTimeout(() => {
      setFilterResetApiInProgress(false);
    }, 300);
    if (
      !res?.data?.success ||
      (res?.data?.success && res?.data?.data?.feed?.items?.length === 0)
    ) {
      // if api call was for getting product feed fails or give empty array on first api call, we will load regular feed
      if (productId.current && pageNumberRef.current === 1) {
        productId.current = undefined;
        getFeed();
        return;
      }
      if (isFreshnessLogicEnabled()) {
        let filteredItemsToShow = itemsToShow.concat(
          itemsPushedDownBecauseOfFreshness.current
        );
        let totalItems;
        if (obj?.isFeedRefreshed) {
          // if it is triggered with filter click and consecutive call is made automatically because of less no of valid records
          totalItems = filteredItemsToShow;
        } else {
          totalItems = feed.items.concat(filteredItemsToShow);
        }
        setFeed({
          ...feed,
          items: totalItems
        });
      }
      setHasMore(false);
      window.postMessage(
        {
          action: "REMOVE_LOADER"
        },
        "*"
      );
      return;
    }
    if (pageNumberRef.current === 1) {
      triggerFeedEvents({
        feedId: storiesProps.feedID,
        setCurrentSessionId: setCurrentSessionId,
        sessionId: currentSessionId,
        lastEvent: window.LAST_EVENT,
        organizationUserId: currentUserId.current,
        userName: currentUserName.current,
        deviceType: rdd.deviceType,
        device: `${rdd.mobileVendor}:${rdd.mobileModel}`,
        browser: `${rdd.browserName}:${rdd.fullBrowserVersion}`,
        osName: rdd.osName?.toUpperCase(),
        osVersion: rdd.osVersion,
        widgetType: "STORIES",
        variantId: storiesProps.variantID || "DEFAULT_FEED",
        appServerUrl: storiesProps.APP_SERVER_URL,
        assetId: storiesProps.assetID,
        authorization: storiesProps.organizationToken,
        domain: window.location.origin
      });
      window.LAST_EVENT = "FEED_LOADED";
      registerBeforeUnload();
    }
    if (isFreshnessLogicEnabled()) {
      const currentSeenAssetIds = getSeenAssetIds(
        props.feedID,
        props.variantID || "DEFAULT_FEED"
      );
      res.data.data.feed.items.forEach((item) => {
        if (currentSeenAssetIds.indexOf(item._id) > -1) {
          itemsPushedDownBecauseOfFreshness.current.push(item);
        } else {
          itemsToShow.push(item);
        }
      });
      pageNumberRef.current = pageNumberRef.current + 1;

      if (itemsToShow.length <= noOfItemsBeforeNextAPI) {
        getFeed({
          itemsToShow,
          dontUpdatePageNo: true,
          isFeedRefreshed: obj?.isFeedRefreshed
        });
        return;
      }
      if (res.data.data.feed.items.length <= noOfItemsBeforeNextAPI) {
        itemsToShow = itemsToShow.concat(
          itemsPushedDownBecauseOfFreshness.current
        );
        itemsPushedDownBecauseOfFreshness.current = [];
        setHasMore(false);
      }
      setFeed({
        ...feed,
        items: obj?.isFeedRefreshed
          ? itemsToShow
          : feed.items.concat(itemsToShow)
      });
      return;
    }
    itemsToShow = res.data.data.feed.items;
    let totalItems;
    if (pageNumberRef.current === 1) {
      setFeed({
        ...feed,
        items: itemsToShow,
        _id: res.data.data.feed._id
      });
      setCurrentActiveItemId(itemsToShow[0]?.items[0]?._id);
      window.postMessage(
        {
          action: "REMOVE_LOADER"
        },
        "*"
      );
    } else {
      if (obj?.isFeedRefreshed) {
        totalItems = itemsToShow;
      } else {
        totalItems = feed.items.concat(itemsToShow);
      }
      setFeed({
        ...feed,
        items: totalItems
      });
    }
    pageNumberRef.current = pageNumberRef.current + 1;
  };

  const getProfileFeed = async () => {
    if (pageNumberRef.current === 1) {
      setCurrentActiveItemId(undefined);
    }
    if (pageNumberRef.current === lastPageRequest.current) {
      return;
    }
    lastPageRequest.current = pageNumberRef.current;
    const res = await getFeedByProfile(
      storiesProps.APP_SERVER_URL,
      storiesProps.feedID,
      storiesProps.profileID,
      storiesProps.organizationToken,
      pageNumberRef.current,
      itemsPerPage,
      false,
      storiesProps.filter
    );
    if (res?.data?.success) {
      if (pageNumberRef.current === 1) {
        if (pageNumberRef.current === 1) {
          triggerProfileEvents({
            profileId: storiesProps.profileID,
            authorization: storiesProps.organizationToken,
            setCurrentSessionId: setCurrentSessionId,
            sessionId: currentSessionId,
            lastEvent: window.LAST_EVENT,
            feedId: storiesProps.feedID,
            organizationUserId: currentUserId.current,
            userName: currentUserName.current,
            deviceType: rdd.deviceType,
            device: `${rdd.mobileVendor}:${rdd.mobileModel}`,
            browser: `${rdd.browserName}:${rdd.fullBrowserVersion}`,
            osName: rdd.osName?.toUpperCase(),
            osVersion: rdd.osVersion,
            widgetType: "STORIES",
            variantId: storiesProps.variantID || "DEFAULT_FEED",
            appServerUrl: storiesProps.APP_SERVER_URL,
            domain: window.location.origin
          });
          registerBeforeUnload();
          window.postMessage(
            {
              action: "REMOVE_LOADER"
            },
            "*"
          );
        }
        setFeed({
          ...feed,
          ...res.data.data.feed
        });
        setProfileMeta({
          totalPosts: res.data.data.feed.totalPosts,
          totalLikes: 0
        });
        setCurrentActiveItemId(res.data.data.feed.items[0]?.items[0]?._id);
      } else {
        if (
          feed.items[feed.items.length - 1]._id !=
          res.data.data.feed.items[res.data.data.feed.items.length - 1]._id
        ) {
          setFeed({
            ...feed,
            items: feed.items.concat(res.data.data.feed.items)
          });
        }
      }

      pageNumberRef.current = pageNumberRef.current + 1;
    } else {
      window.postMessage(
        {
          action: "REMOVE_LOADER"
        },
        "*"
      );
    }
  };

  const getIndividualAsset = async () => {
    const response = await getFeedByAsset(
      storiesProps.APP_SERVER_URL,
      storiesProps.assetID,
      storiesProps.organizationToken,
      false
    );
    if (response?.data?.success) {
      let newArray = [];
      if (
        response.data.data.asset?.items &&
        response.data.data.asset?.items?.length > 0
      ) {
        newArray = [...newArray, response?.data?.data?.asset];
        setCurrentActiveItemId(response.data.data.asset?.items[0]?._id);
      }
      triggerIndividualAssetEvents({
        feedId: response?.data?.data?.asset?._feed,
        assetId: response?.data?.data?.asset?._id,
        sessionId: currentSessionId,
        setCurrentSessionId: setCurrentSessionId,
        lastEvent: window.LAST_EVENT,
        organizationUserId: currentUserId.current,
        authorization: storiesProps.organizationToken,
        domain: window.location.origin,
        userName: currentUserName.current,
        variantId: storiesProps.variantID || "DEFAULT_FEED",
        deviceType: rdd.deviceType,
        device: `${rdd.mobileVendor}:${rdd.mobileModel}`,
        browser: `${rdd.browserName}:${rdd.fullBrowserVersion}`,
        osName: rdd.osName?.toUpperCase(),
        osVersion: rdd.osVersion,
        widgetType: "STORIES",
        appServerUrl: storiesProps.APP_SERVER_URL
      });
      registerBeforeUnload();
      window.postMessage(
        {
          action: "REMOVE_LOADER"
        },
        "*"
      );

      let resp = await getPublicFeed(
        storiesProps.APP_SERVER_URL,
        response.data?.data?.asset?._feed,
        storiesProps.organizationToken,
        pageNumberRef.current,
        itemsPerPage,
        false,
        storiesProps.filter,
        storiesProps.assetID
      );
      if (resp?.data?.success) {
        if (resp.data.data.feed.items.length === 0) {
          setHasMore(false);
          setFeed({
            items: feed.items.concat(newArray),
            _id: resp.data.data?.feed?._id
          });
        } else {
          newArray = [...newArray, ...resp.data.data.feed.items];
          setFeed({
            items: feed.items.concat(newArray),
            _id: resp.data.data?.feed?._id
          });
        }
        pageNumberRef.current = pageNumberRef.current + 1;
      }
    }
  };

  return (
    <StoriesContext.Provider
      value={{
        getFeed,
        feed,
        storiesProps,
        setStoriesProps,
        organizationDetails,
        itemsPerPage,
        setOrganizationDetails,
        currentSessionId,
        setCurrentSessionId,
        postAnalyticsAPI,
        markAssetAsViewed,
        getProfileFeed,
        profileMeta,
        getIndividualAsset,
        hasMore,
        currentActiveItemId,
        setCurrentActiveItemId,
        viewedAssetsRef,
        nativeTapTriggered,
        setNativeTapTriggered,
        containerFeedWidth,
        setContainerFeedWidth,
        feedTheme,
        setFeedTheme,
        setHasMore,
        noOfItemsBeforeNextAPI,
        viewedAssetsRef,
        filterResetApiInProgress,
        setFilterResetApiInProgress,
        hasUserChangedSlide,
        setHasUserChangedSlide,
        showProfile,
        setShowProfile,
        carouselContainerWidth,
        setCarouselContainerWidth
      }}
    >
      {props.children}
    </StoriesContext.Provider>
  );
};
