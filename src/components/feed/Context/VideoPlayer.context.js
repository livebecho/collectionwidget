import React, { createContext, useEffect, useState } from "react";
import canAutoPlay from "can-autoplay";
export const VideoPlayerContext = createContext();

export default (props) => {
  const [currentPlayedAssetIds, setCurrentPlayedAssetIds] = useState([]);
  const [isMuted, setIsMuted] = useState(true);
  const [userInteracted, setUserInteracted] = useState(false);
  const [userPlayedAsset, setUserPlayedAsset] = useState(undefined);
  const [fullScreenID, setFullScreenID] = useState(undefined);
  const [isHlsLoaded, setIsHlsLoaded] = useState(false);
  const checkIfAutoplayEnabled = async () => {
    if (props.isStories) {
      const result = await canAutoPlay.video();
      if (result.result) {
        setIsMuted(false);
        setUserInteracted(true);
      }
    }
  };

  useEffect(() => {
    checkIfAutoplayEnabled();
  }, []);

  return (
    <VideoPlayerContext.Provider
      value={{
        currentPlayedAssetIds,
        setCurrentPlayedAssetIds,
        setIsMuted: setIsMuted,
        isMuted,
        userPlayedAsset,
        setUserPlayedAsset,
        setFullScreenID,
        fullScreenID,
        userInteracted,
        setUserInteracted,
        isHlsLoaded,
        setIsHlsLoaded
      }}
    >
      {props.children}
    </VideoPlayerContext.Provider>
  );
};
