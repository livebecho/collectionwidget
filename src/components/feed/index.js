import React, { Suspense } from "react";
import "navigator.sendbeacon";
import InterceptionManager from "./Wrappers/Interceptors";
import SetupAndRender from "./Wrappers/SetupAndRender";
import StoriesContextProvider from "./Context/Stories.context";
import "./index.css";

function App(props) {
  return (
    <StoriesContextProvider {...props}>
      <Suspense fallback={<div></div>}>
        <InterceptionManager {...props} />
        <SetupAndRender {...props} />
      </Suspense>
    </StoriesContextProvider>
  );
}
export default App;
