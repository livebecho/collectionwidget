import React, { useRef, useEffect, useState, useContext } from "react";
import Styles from "./VideoPlayer.module.scss";
import { StoriesContext } from "../../../Context/Stories.context";
import { VideoPlayerContext } from "../../../Context/VideoPlayer.context";
import uniqid from "uniqid";
import axios from "axios";
import * as rdd from "react-device-detect";
import ReactHlsPlayer from "../../../../../ReactHlsPlayer";

export default (props) => {
  const videoPlayer = useContext(VideoPlayerContext);
  const session = useContext(StoriesContext);
  const playerRef = useRef();
  const videoRepeatCount = useRef(0);
  const itemRef = useRef();
  const [isPauseInProgress, setIsPauseInProgress] = useState(undefined);
  const videoPlayerIntanceId = useRef(uniqid());
  itemRef.current = props.item;
  const [initializePlayer, setInitializePlayer] = useState(false);
  const initializePlayerRef = useRef(initializePlayer);

  useEffect(() => {
    if (props.initializePlayer && !initializePlayerRef.current) {
      initializePlayerRef.current = true;
      setInitializePlayer(true);
    }
  }, [props.initializePlayer]);

  useEffect(() => {
    if (playerRef.current) {
      playerRef.current.volume = props.isMuted ? 0 : 1;
      playerRef.current.muted = props.isMuted;
    }
  }, [props.isMuted, playerRef.current]);

  useEffect(() => {
    const player = playerRef.current;
    if (!player) {
      return;
    }
    if (
      itemRef.current &&
      props.currentActiveItemId &&
      itemRef.current._id == props.currentActiveItemId &&
      !session.filterResetApiInProgress &&
      !isPauseInProgressRef.current
    ) {
      if (player.paused) {
        // for ios as ios doesnt work properly if feed goes in background and then play button is pressed
        if (player.currentTime != 0) {
          if (player.currentTime >= 1) {
            player.currentTime =
              Math.round(player.currentTime) === player.currentTime
                ? Math.round(player.currentTime)
                : Math.round(player.currentTime) - 1;
          } else {
            player.currentTime = player.currentTime === 0 ? 1 : 0;
          }
        }
        player.play().catch((e) => {
          if (
            e.message.indexOf(
              "The play() request was interrupted by a call to pause"
            ) > -1
          ) {
            return;
          }
          sendErrorEmail(e.message, 15);
          setTimeout(() => {
            props.setIsMuted(true);
            videoPlayer.setUserInteracted(false);
            player.muted = true;
            player.volume = 0;
            player.play().catch((e) => {
              sendErrorEmail(e.message, 16);
            });
          }, 0);
        });
        session.postAnalyticsAPI("VIDEO_PLAY", {
          videoPlayerInstanceId: videoPlayerIntanceId.current,
          assetId: session.feed?.items[props.assetIndex]?._id,
          itemId: props?.item?._id,
          currentTimestamp: player.currentTime,
          totalVideoDuration: player.duration,
          profileName: session.feed?.items[props.assetIndex]?._createdBy?.name,
          profileId: session.feed?.items[props.assetIndex]?._createdBy?._id
        });
      }
    } else {
      if (!player.paused) {
        player.pause();
      }
    }
  }, [
    isPauseInProgress,
    initializePlayer,
    playerRef.current,
    itemRef.current,
    props.currentActiveItemId,
    session.filterResetApiInProgress
  ]);

  const isPauseInProgressRef = useRef(isPauseInProgress);

  useEffect(() => {
    const player = playerRef.current;
    if (!player) {
      return;
    }
    if (
      itemRef.current &&
      props.currentActiveItemId &&
      itemRef.current._id == props.currentActiveItemId &&
      !session.filterResetApiInProgress
    ) {
      try {
        if (player.paused && !isPauseInProgressRef.current) {
          player
            .play()
            .then(() => {
              session.postAnalyticsAPI("VIDEO_PLAY", {
                videoPlayerInstanceId: videoPlayerIntanceId.current,
                assetId: session.feed?.items[props.assetIndex]?._id,
                itemId: props?.item?._id,
                currentTimestamp: player.currentTime,
                totalVideoDuration: player.duration,
                profileName:
                  session.feed?.items[props.assetIndex]?._createdBy?.name,
                profileId:
                  session.feed?.items[props.assetIndex]?._createdBy?._id
              });
            })
            .catch((e) => {
              sendErrorEmail(e.message, 1);
              setTimeout(() => {
                console.log("muting 2");
                props.setIsMuted(true);
                videoPlayer.setUserInteracted(false);
                player.muted = true;
                player.volume = 0;
                player
                  .play()
                  .then(() => {
                    session.postAnalyticsAPI("VIDEO_PLAY", {
                      videoPlayerInstanceId: videoPlayerIntanceId.current,
                      assetId: session.feed?.items[props.assetIndex]?._id,
                      itemId: props?.item?._id,
                      currentTimestamp: player.currentTime,
                      totalVideoDuration: player.duration,
                      profileName:
                        session.feed?.items[props.assetIndex]?._createdBy?.name,
                      profileId:
                        session.feed?.items[props.assetIndex]?._createdBy?._id
                    });
                  })
                  .catch((e) => {
                    console.log(e);
                    sendErrorEmail(e.message, 2);
                  });
              }, 0);
            });
        }
      } catch (e) {
        console.log("somethin wrong", itemRef.current._id);
        sendErrorEmail(e.message, 3);
      }
    } else {
      try {
        if (!player.paused) {
          session.postAnalyticsAPI("VIDEO_PAUSE", {
            assetId: session.feed?.items[props.assetIndex]?._id,
            itemId: props?.item?._id,
            currentTimestamp: player.currentTime,
            totalVideoDuration: player.duration,
            profileName:
              session.feed?.items[props.assetIndex]?._createdBy?.name,
            profileId: session.feed?.items[props.assetIndex]?._createdBy?._id,
            videoPlayerInstanceId: videoPlayerIntanceId.current
          });
          setIsPauseInProgress(true);
          player.pause();
          player.currentTime = 0;
        }
      } catch (e) {
        console.log("somethin wrong", itemRef.current._id);
        sendErrorEmail(e.message, 4);
      }
    }
  }, [
    itemRef.current,
    props.currentActiveItemId,
    playerRef.current,
    session.filterResetApiInProgress
  ]);

  function timeUpdateHandler(e) {
    const player = playerRef.current;
    const playerCurrentTime = parseInt(player.currentTime);
    if (playerCurrentTime === 3) {
      let isShoppable = false;
      if (
        props?.item?.hotspots?.length > 0 ||
        props?.item?.metaData?.taggedProducts?.length > 0 ||
        props?.item?.metaData?.customLinks?.length > 0
      ) {
        isShoppable = true;
      }
      session.markAssetAsViewed("VIDEO_SEEN", {
        isShoppable: isShoppable,
        assetId: session.feed?.items[props.assetIndex]?._id,
        itemId: props?.item?._id,
        profileName: session.feed?.items[props.assetIndex]?._createdBy?.name,
        profileId: session.feed?.items[props.assetIndex]?._createdBy?._id
      });
    }
  }
  const moveToNextSlideRef = useRef(props.moveToNextSlide);
  moveToNextSlideRef.current = props.moveToNextSlide;

  const intervalId = useRef(undefined);
  const lastTime = useRef(undefined);

  useEffect(() => {
    const player = playerRef.current;
    if (
      itemRef.current &&
      props.currentActiveItemId &&
      itemRef.current._id == props.currentActiveItemId &&
      moveToNextSlideRef.current &&
      !session.filterResetApiInProgress &&
      player
    ) {
      if (!intervalId.current) {
        intervalId.current = setInterval(() => {
          const playerDuration = player.duration;
          const currentTime = player.currentTime;
          props.updateProgressBarRef((currentTime / playerDuration) * 100);
        }, 60);
      }
    } else {
      if (intervalId.current) {
        clearInterval(intervalId.current);
        intervalId.current = undefined;
      }
    }
  }, [
    itemRef.current,
    props.currentActiveItemId,
    playerRef.current,
    session.filterResetApiInProgress
  ]);

  function onEnded() {
    try {
      if (moveToNextSlideRef.current) {
        moveToNextSlideRef.current();
      } else {
        playerRef.current.play().catch((e) => {
          sendErrorEmail(e.message, 11);
          setTimeout(() => {
            console.log("muting 3");
            props.setIsMuted(true);
            videoPlayer.setUserInteracted(false);
            playerRef.current.muted = true;
            playerRef.current.volume = 0;
            playerRef.current.play().catch((e) => {
              sendErrorEmail(e.message, 12);
              console.log(e);
            });
          }, 0);
        });
        videoRepeatCount.current = videoRepeatCount.current + 1;
        if (videoRepeatCount.current < 3) {
          session.postAnalyticsAPI("VIDEO_REPEAT", {
            assetId: session.feed?.items[props.assetIndex]?._id,
            itemId: props?.item?._id,
            profileName:
              session.feed?.items[props.assetIndex]?._createdBy?.name,
            profileId: session.feed?.items[props.assetIndex]?._createdBy?._id,
            currentTimestamp: playerRef.current?.currentTime,
            totalVideoDuration: playerRef.current?.duration,
            videoPlayerInstanceId: videoPlayerIntanceId.current
          });
        }
      }
    } catch (e) {
      sendErrorEmail(e.message, 13);
      console.log("something messed up", props.item._id);
    }
  }

  const sendErrorEmail = async (error, number) => {
    if (rdd.osName?.toUpperCase() != "IOS") {
      return;
    }
    await axios.post(
      `${session.storiesProps.APP_SERVER_URL}/api/v1/video-error-email`,
      {
        error: { message: error, flag: number },
        deviceDetails: {
          deviceType: rdd.deviceType,
          device: `${rdd.mobileVendor}:${rdd.mobileModel}`,
          browser: `${rdd.browserName}:${rdd.fullBrowserVersion}`,
          osName: rdd.osName?.toUpperCase(),
          osVersion: rdd.osVersion,
          isMuted: props.isMuted,
          codeVersion: 1
        },
        widgetType: "STORIES",
        itemDetails: props.item,
        postDetails: session.feed?.items[props.assetIndex]
      }
    );
  };

  return (
    <div className={Styles.videoContainer}>
      <div className={Styles.videoSection}>
        {initializePlayer && (
          <ReactHlsPlayer
            src={props.item?.hlsKey}
            playerRef={playerRef}
            width="100%"
            height="auto"
            preload="none"
            playsInline
            crossOrigin={"anonymous"}
            muted={props.isMuted ? "muted" : undefined}
            poster={props.item?.thumbnail}
            onTimeUpdateCapture={(ev) => {
              timeUpdateHandler(ev);
            }}
            onPlay={() => {
              const player = playerRef.current;
              if (!player) {
                return;
              }
              if (
                itemRef.current &&
                props.currentActiveItemId &&
                itemRef.current._id == props.currentActiveItemId &&
                !session.filterResetApiInProgress &&
                !isPauseInProgressRef.current
              ) {
                player.play();
              } else {
                if (!player.paused) {
                  player.pause();
                }
              }
            }}
            onEnded={() => {
              onEnded();
            }}
          />
        )}
      </div>
    </div>
  );
};
