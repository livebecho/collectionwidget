import React, { useContext, useEffect, useRef } from "react";
import Styles from "./StoryThumbnail.module.scss";
import { StoriesContext } from "../../../Context/Stories.context";

function StoryThumbnail({ setStoryItemSelected, setCarouselActiveItemIndex }) {
  const session = useContext(StoriesContext);

  const canvasRef = useRef([]);

  const createCircleForStory = (canvas, numberOfItems) => {
    const ctx = canvas.getContext("2d");
    if (numberOfItems === 1) {
      ctx.beginPath();
      ctx.lineWidth = 3;
      ctx.arc(40, 40, 38, 0, (360 * Math.PI) / 180);
      ctx.strokeStyle = "green";
      ctx.stroke();
    } else {
      let startingValue = 0;
      for (let i = 0; i < numberOfItems; i++) {
        const endingDegree = startingValue + 360 / numberOfItems - 5;
        ctx.beginPath();
        ctx.lineWidth = 3;
        ctx.arc(
          40,
          40,
          38,
          (startingValue * Math.PI) / 180,
          (endingDegree * Math.PI) / 180
        );
        ctx.strokeStyle = "green";
        ctx.stroke();
        startingValue = (360 / numberOfItems) * (i + 1);
      }
    }
  };

  useEffect(() => {
    window.addEventListener("popstate", () => {
      setStoryItemSelected(false);
      setCarouselActiveItemIndex(0);
    });
    return () => {
      window.removeEventListener("popstate", () => {
        setStoryItemSelected(false);
        setCarouselActiveItemIndex(0);
      });
    };
  }, []);

  useEffect(() => {
    if (canvasRef && canvasRef?.current) {
      for (let i = 0; i < canvasRef?.current?.length; i++) {
        createCircleForStory(
          canvasRef?.current[i],
          session.feed?.items[i].items.length
        );
      }
    }
  }, [canvasRef?.current?.length]);

  return (
    <div className={Styles.storyThumbnailContainer}>
      {session.feed?.items.length > 0 &&
        session.feed?.items.map((item, index) => {
          return (
            <div
              className={Styles.eachStoryItemContainer}
              onClick={() => {
                setStoryItemSelected(true);
                setCarouselActiveItemIndex(index);
                window.history.pushState(
                  { hideGrid: true },
                  "",
                  window.location.href
                );
              }}
              key={index}
            >
              <div className={Styles.imageContainer}>
                <div className={Styles.thumbContainer}>
                  <img src={item?.items[0]?.thumbnail} />
                </div>
              </div>
              <canvas
                id="borderCanvas"
                className={Styles.borderCanvas}
                ref={(ref) => (canvasRef.current[index] = ref)}
                width="80"
                height="80"
              ></canvas>
            </div>
          );
        })}
    </div>
  );
}

export default StoryThumbnail;
