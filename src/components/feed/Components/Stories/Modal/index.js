import React from "react";
import ReactDOM from "react-dom";
import Styles from "./Modal.module.scss";

export default ({ children, onClose }) => {
  return ReactDOM.createPortal(
    <div className={Styles.modal} style={{}}>
      <div className={Styles.backdrop} onClick={onClose}></div>
      {children}
    </div>,
    document.body
  );
};
