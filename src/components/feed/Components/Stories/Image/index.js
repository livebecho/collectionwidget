import React, { useEffect, useContext, useRef } from "react";
import Styles from "./Image.module.scss";
import { StoriesContext } from "../../../Context/Stories.context";

const StoriesImageContainer = (props) => {
  let { item, active, assetIndex } = props;
  const session = useContext(StoriesContext);
  const timeoutIdRef = useRef(undefined);
  const intervalId = useRef(undefined);
  const lastTime = useRef(2);
  const moveToNextSlideRef = useRef(props.moveToNextSlide);
  moveToNextSlideRef.current = props.moveToNextSlide;

  useEffect(() => {
    if (active && moveToNextSlideRef.current) {
      if (!intervalId.current) {
        lastTime.current = 2;
        intervalId.current = setInterval(() => {
          props.updateProgressBarRef(lastTime.current);
          lastTime.current = lastTime.current + 2;
        }, 60);
      }
      if (!timeoutIdRef.current) {
        timeoutIdRef.current = setTimeout(() => {
          moveToNextSlideRef.current();
        }, 3000);
      }
    } else {
      if (timeoutIdRef.current) {
        clearTimeout(timeoutIdRef.current);
        timeoutIdRef.current = undefined;
      }
      if (intervalId.current) {
        clearInterval(intervalId.current);
        intervalId.current = undefined;
      }
    }
  }, [active]);

  useEffect(() => {
    if (active) {
      let isShoppable = false;
      if (
        item?.hotspots?.length > 0 ||
        item?.metaData?.taggedProducts?.length > 0 ||
        item?.metaData?.customLinks?.length > 0
      ) {
        isShoppable = true;
      }
      session.markAssetAsViewed("IMAGE_SEEN", {
        isShoppable: isShoppable,
        assetId: session.feed?.items[assetIndex]?._id,
        itemId: item._id,
        profileName: session.feed?.items[assetIndex]?._createdBy?.name,
        profileId: session.feed?.items[assetIndex]?._createdBy?._id
      });
    }
  }, [active, item, session.feed?.items, assetIndex]);

  return (
    <div className={Styles.imageContainer}>
      <img
        src={item.key}
        onError={(e) => {
          if (decodeURI(e.target.src) !== item.keyFallback) {
            e.target.onerror = null;
            e.target.src = item.keyFallback;
          }
        }}
      />
    </div>
  );
};

export default (props) => {
  return <StoriesImageContainer {...props} />;
};
