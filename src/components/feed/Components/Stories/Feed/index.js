import React, { useState, useContext, useEffect, useRef } from "react";
import canAutoPlay from "can-autoplay";
import Styles from "./Feed.module.scss";
import { Carousel } from "react-responsive-carousel";
import "react-responsive-carousel/lib/styles/carousel.min.css";
import FeedItemComponent from "../FeedItem";
import VideoPlayerContextProvider, {
  VideoPlayerContext
} from "../../../Context/VideoPlayer.context";
import { StoriesContext } from "../../../Context/Stories.context";
import StoryThumbnail from "../StoryThumbnail";
import LeftArrow from "../../../../../assets/images/leftArrow.svg";
import RightArrow from "../../../../../assets/images/rightArrow.svg";

const width = document.body.getBoundingClientRect().width;
const FeedComponent = (props) => {
  const IsBackgroundEventTriggeredRef = useRef(false);
  const hotspottriggeridmap = useRef({});
  const session = useContext(StoriesContext);
  const videoPlayer = useContext(VideoPlayerContext);

  function loadScript(url, callback) {
    var script = document.createElement("script");
    script.type = "text/javascript";
    if (script.readyState) {
      script.onreadystatechange = function () {
        if (
          script.readyState === "loaded" ||
          script.readyState === "complete"
        ) {
          script.onreadystatechange = null;
          callback();
        }
      };
    } else {
      script.onload = function () {
        callback();
      };
    }

    script.src = url;
    document.getElementsByTagName("head")[0].appendChild(script);
  }

  useEffect(() => {
    loadScript(
      "https://reels-widget.livebecho.com/vendor/hls/dist/hls.light.min.js",
      function () {
        videoPlayer.setIsHlsLoaded(true);
      }
    );
  }, []);

  // QUESTIONS: setActiveItemIndex - why this is here, should be in feeditem?
  const [options, setOptions] = useState({
    share: true,
    full_screen: true
  });
  const [carouselContainerWidth, setCarouselContainerWidth] = useState();
  const noOfItemsBeforeNextAPI = session.noOfItemsBeforeNextAPI;
  const [userAuthenticatedTracked, setUserAuthenticatedTracked] =
    useState(false);
  const [carouselActiveItemIndex, setCarouselActiveItemIndex] = useState(
    props.activeItemIndex || 0
  );
  const [storyItemSelected, setStoryItemSelected] = useState(false);
  const [storyThumbnailRefDetails, setStoryThumbnailRefDetails] = useState({
    width: 0,
    scrollWidth: 0,
    leftScroll: 0
  });

  useEffect(() => {
    if (typeof props.activeItemIndex != "undefined") {
      setCarouselActiveItemIndex(props.activeItemIndex);
    }
  }, [props.activeItemIndex]);

  const sessionItems = useRef();
  sessionItems.current = session.feed?.items;

  const moveToNextSlide = () => {
    setCarouselActiveItemIndex(carouselActiveItemIndex + 1);
  };
  const moveToPreviousSlide = () => {
    setCarouselActiveItemIndex(carouselActiveItemIndex - 1);
  };

  const currentActiveItemId = session.currentActiveItemId;

  const messageHandler = async (evt) => {
    if (evt.data) {
      if (evt.data.action === "SET_USER_ID") {
        session.setOrganizationDetails({
          ...session.organizationDetails,
          userID: evt.data.payload.userID,
          userName: evt.data.payload.userName
        });
      } else if (evt.data.action === "SET_ORGANIZATION_INFO") {
        session.setOrganizationDetails({
          ...session.organizationDetails,
          userID: evt.data.payload.userID,
          userName: evt.data.payload.userName
        });
      } else if (evt.data.action === "SET_OPTIONS") {
        setOptions(evt.data.payload.options);
      } else if (evt.data.action === "PAUSE_PLAYERS") {
        videoPlayer.setIsMuted(true);
      } else if (evt.data.action === "FEED_IN_BACKGROUND") {
        if (!IsBackgroundEventTriggeredRef.current) {
          IsBackgroundEventTriggeredRef.current = true;
          manageUnload();
          videoPlayer.setIsMuted(true);
        }
      } else if (evt.data.action === "FEED_IN_FOCUS") {
        if (IsBackgroundEventTriggeredRef.current) {
          manageLoad();
          const result = await canAutoPlay.video();
          if (result.result) {
            videoPlayer.setIsMuted(false);
          }
          IsBackgroundEventTriggeredRef.current = false;
        }
      }
    }
  };

  useEffect(() => {
    window.addEventListener("message", messageHandler);
    window.postMessage(
      {
        action: "PAGE_LOADED"
      },
      "*"
    );
    return () => {
      window.removeEventListener("message", messageHandler);
    };
  }, [session.organizationDetails, currentActiveItemId]);

  useEffect(() => {
    if (
      session.organizationDetails?.userID &&
      props.organizationToken &&
      session.feed?.items?.length > 0 &&
      !userAuthenticatedTracked
    ) {
      session.postAnalyticsAPI("USER_AUTHENTICATED", {});
      setUserAuthenticatedTracked(true);
    }
  }, [
    session.organizationDetails?.userID,
    props.organizationToken,
    session.feed,
    userAuthenticatedTracked
  ]);

  const getStoryThumbnailRefDetails = (details) => {
    if (!details) {
      return;
    }
    setStoryThumbnailRefDetails({
      width: details?.offsetWidth,
      scrollWidth: details?.scrollWidth,
      leftScroll: details?.scrollLeft
    });
  };

  useEffect(() => {
    const storyThumbnailId = document.getElementById("storyThumbnailSection");
    if (storyThumbnailId) {
      getStoryThumbnailRefDetails(storyThumbnailId);
      storyThumbnailId.addEventListener("scroll", (e) => {
        getStoryThumbnailRefDetails(e?.target);
      });
      window.addEventListener("resize", (e) => {
        getStoryThumbnailRefDetails(storyThumbnailId);
      });
    }

    return () => {
      if (storyThumbnailId) {
        storyThumbnailId.removeEventListener("scroll");
        window.removeEventListener("resize");
      }
    };
  }, [session]);

  let itemsToRender = session.feed.items;
  const startPointRef = useRef();

  const manageUnload = () => {
    if (window?._livebecho?.currentPage === "FEED_PAGE") {
      session.postAnalyticsAPI("FEED_UNLOADED", {});
    } else if (window?._livebecho?.currentPage === "PROFILE_PAGE") {
      session.postAnalyticsAPI("PROFILE_UNLOADED", {
        profileId: session.feed?.items[carouselActiveItemIndex]?._createdBy?._id
      });
    }
  };

  const manageLoad = () => {
    if (window?._livebecho?.currentPage === "FEED_PAGE") {
      session.postAnalyticsAPI("FEED_LOADED", {});
    } else if (window?._livebecho?.currentPage === "PROFILE_PAGE") {
      session.postAnalyticsAPI("PROFILE_LOADED", {
        profileId: session.feed?.items[carouselActiveItemIndex]?._createdBy?._id
      });
    }
  };

  return (
    <>
      {props.hideStories ? (
        <></>
      ) : (
        <div className={Styles.feedSection}>
          <div
            ref={(div) => {
              if (div) {
                const width = (div.getBoundingClientRect().height * 9) / 16;
                session.setContainerFeedWidth(width);
                setCarouselContainerWidth(width);
              }
            }}
            className={Styles.container}
          >
            {storyItemSelected ? (
              <div
                id="carouselContainer"
                className={Styles.carouselContainer}
                style={{ width: carouselContainerWidth + "px" }}
              >
                {!session?.filterResetApiInProgress &&
                  session.feed?.items.length > 0 && (
                    <Carousel
                      swipeable
                      selectedItem={carouselActiveItemIndex}
                      autoPlay={false}
                      stopOnHover={false}
                      interval={600000}
                      emulateTouch
                      axis="horizontal"
                      transitionTime={150}
                      showStatus={false}
                      swipeScrollTolerance={5} // dont remove or reduce this as its important for video to play
                      onSwipeStart={(event) => {
                        if (
                          carouselActiveItemIndex === 0 &&
                          event.type === "touchstart"
                        ) {
                          startPointRef.current = event.touches[0].clientY;
                        }
                      }}
                      onSwipeMove={(event) => {
                        if (event.type === "touchmove") {
                          if (
                            carouselActiveItemIndex === 0 &&
                            startPointRef.current < event.touches[0].clientY
                          ) {
                            if (props.moveToTheTop) {
                              props.moveToTheTop();
                            }
                          }
                        }
                      }}
                      onChange={async (index) => {
                        if (!session.hasUserChangedSlide) {
                          session.setHasUserChangedSlide(true);
                        }
                        if (carouselActiveItemIndex === index) {
                          return;
                        }
                        if (!videoPlayer.userInteracted) {
                          const result = await canAutoPlay.video();
                          if (result.result) {
                            videoPlayer.setIsMuted(false);
                            videoPlayer.setUserInteracted(true);
                          }
                        }
                        if (index === 0 || index === 1) {
                          if (props.setOverflow) {
                            props.setOverflow(index === 0);
                          }
                        }
                        setCarouselActiveItemIndex(index);
                        if (
                          itemsToRender.length - index <=
                          noOfItemsBeforeNextAPI
                        ) {
                          if (!session.hasMore) {
                            return;
                          }
                          if (
                            session?.storiesProps?.currentPage ===
                            "PROFILE_PAGE"
                          ) {
                            session.getProfileFeed();
                            return;
                          }
                          if (
                            session?.storiesProps?.currentPage ===
                            "INDIVIDUAL_ASSET_PAGE"
                          ) {
                            session.getIndividualAsset();
                            return;
                          }
                          session.getFeed();
                        }
                      }}
                      showThumbs={false}
                      showArrows={false}
                      showIndicators={false}
                    >
                      {itemsToRender?.map((obj, index) => {
                        return (
                          <div key={index} className={Styles.stories}>
                            <FeedItemComponent
                              moveToNextVerticalSlide={moveToNextSlide}
                              moveToPreviousVerticalSlide={moveToPreviousSlide}
                              setHotspotTriggerIdMap={(itemid, id) => {
                                hotspottriggeridmap.current = {
                                  ...hotspottriggeridmap.current,
                                  [itemid]: id
                                };
                              }}
                              currentCarouselActiveIndex={
                                carouselActiveItemIndex
                              }
                              width={width}
                              index={index}
                              options={options}
                            />
                          </div>
                        );
                      })}
                    </Carousel>
                  )}
                {/* {!props.fullView && (
                <div className={Styles.overlayToPreventSwiping}></div>
              )} */}
              </div>
            ) : (
              <div
                className={Styles.storyThumbnailSection}
                id="storyThumbnailSection"
              >
                <StoryThumbnail
                  setStoryItemSelected={setStoryItemSelected}
                  setCarouselActiveItemIndex={setCarouselActiveItemIndex}
                />
                {storyThumbnailRefDetails?.leftScroll > 0 && (
                  <div
                    className={Styles.swipingArrow}
                    style={{ left: "25%" }}
                    onClick={() => {
                      const storyThumbnailId = document.getElementById(
                        "storyThumbnailSection"
                      );
                      storyThumbnailId.scrollLeft =
                        storyThumbnailId?.scrollLeft - 150;
                    }}
                  >
                    <img src={LeftArrow} />
                  </div>
                )}
                {storyThumbnailRefDetails?.width <
                  storyThumbnailRefDetails?.scrollWidth &&
                  storyThumbnailRefDetails?.width +
                    storyThumbnailRefDetails.leftScroll +
                    10 <
                    storyThumbnailRefDetails?.scrollWidth && (
                    <div
                      className={Styles.swipingArrow}
                      style={{ right: "25%" }}
                      onClick={() => {
                        const storyThumbnailId = document.getElementById(
                          "storyThumbnailSection"
                        );
                        storyThumbnailId.scrollLeft =
                          storyThumbnailId?.scrollLeft + 150;
                      }}
                    >
                      <img src={RightArrow} />
                    </div>
                  )}
              </div>
            )}
          </div>
          <style>
            {`
          @keyframes livebecho_stories_feed_itemIndicatorContainer_itemIndicatorInactive_progressAnimation {
            from {
              transform: scaleX(0);
              background: white;
            }
            to {
              transform: scaleX(1);
              background: white;
            }
          }`}
          </style>
        </div>
      )}
    </>
  );
};

export default (props) => {
  return (
    <VideoPlayerContextProvider isStories>
      <FeedComponent {...props} isStories />
    </VideoPlayerContextProvider>
  );
};
