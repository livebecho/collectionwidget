import React, { useState, useContext, useEffect, useRef } from "react";
import Styles from "./FeedItem.module.scss";
import StoriesImage from "../Image";
import StoriesVideoPlayer from "../VideoPlayer";
import { VideoPlayerContext } from "../../../Context/VideoPlayer.context";
import { StoriesContext } from "../../../Context/Stories.context";
import ProfileImage from "./assets/images/DefaultProfileImage.png";
import NoPosts from "./assets/images/noPosts.png";
import { Carousel } from "react-responsive-carousel";
import "react-responsive-carousel/lib/styles/carousel.min.css";

const FeedItemComponent = ({
  index,
  currentCarouselActiveIndex,
  moveToNextVerticalSlide,
  moveToPreviousVerticalSlide
}) => {
  const session = useContext(StoriesContext);
  const shouldInitializePlayer = (index) => {
    if (!session.hasUserChangedSlide) {
      return currentCarouselActiveIndex === index;
    }
    return (
      currentCarouselActiveIndex + 2 > index &&
      currentCarouselActiveIndex - 2 < index
    );
  };

  const indexRef = useRef(index);
  const progressBarRefs = useRef([]);
  const videoPlayer = useContext(VideoPlayerContext);
  const [activeItemIndex, setActiveItemIndex] = useState(0);
  const activeIndexRef = useRef(activeItemIndex);
  activeIndexRef.current = activeItemIndex;
  const [showDescription, setShowDescription] = useState(false);
  const currentItemObjectRef = useRef(undefined);
  const currentActiveItemId = session.currentActiveItemId;

  const sessionRef = useRef(session);
  sessionRef.current = session;

  useEffect(() => {
    if (currentCarouselActiveIndex == index) {
      session.setCurrentActiveItemId(
        sessionRef.current.feed.items[index].items[activeIndexRef.current]._id
      );
    }

    return () => (progressBarRefs.current = []);
  }, [currentCarouselActiveIndex]);

  const moveToNextSlide = moveToNextVerticalSlide
    ? () => {
        if (progressBarRefs.current[activeItemIndex]) {
          if (
            activeItemIndex ===
            sessionRef.current.feed.items[indexRef.current].items.length - 1
          ) {
            moveToNextVerticalSlide();
          } else {
            setActiveItemIndex(activeItemIndex + 1);
            session.setCurrentActiveItemId(
              sessionRef.current.feed.items[indexRef.current].items[
                activeItemIndex + 1
              ]._id
            );
          }
        }
      }
    : undefined;

  currentItemObjectRef.current = session.feed?.items[index]?.items?.find(
    (curr) => {
      return JSON.stringify(curr._id) === JSON.stringify(currentActiveItemId);
    }
  );

  const manageUnload = () => {
    if (window?._livebecho?.currentPage === "FEED_PAGE") {
      session.postAnalyticsAPI("FEED_UNLOADED", {});
    } else if (window?._livebecho?.currentPage === "PROFILE_PAGE") {
      session.postAnalyticsAPI("PROFILE_UNLOADED", {
        profileId: session.feed?.items[index]?._createdBy?._id
      });
    }
  };

  const unloadingAnalytics = () => {
    if (
      window?._livebecho?.version === 0.2 &&
      window?._livebecho?.OPEN_PROFILE?.callback
    ) {
      manageUnload();
    }
  };

  const openExternalLink = (url) => {
    manageUnload();
    videoPlayer.setCurrentPlayedAssetIds([]);
    videoPlayer.setIsMuted(true);
    videoPlayer.setUserPlayedAsset(undefined);
    window.postMessage(
      {
        action: "OPEN_EXTERNAL_LINK",
        payload: {
          url: url,
          sessionID: session.currentSessionId,
          analytics: {
            utm_source: "LIVEBECHO",
            utm_medium: session.feed?.items[index]?._createdBy?.name,
            utm_campaign: session.feed?.items[index]?._id,
            user: {
              id: session.organizationDetails.userID,
              name: session.organizationDetails.userName
            }
          }
        }
      },
      "*"
    );
  };

  const openProfile = () => {
    unloadingAnalytics();
    window.postMessage(
      {
        action: "OPEN_PROFILE",
        payload: {
          profileID: session.feed?.items[index]?._createdBy?._id,
          sessionID: session.currentSessionId,
          analytics: {
            utm_source: "LIVEBECHO",
            utm_medium: session.feed?.items[index]?._createdBy?.name,
            utm_campaign: session.feed?.items[index]?._id,
            user: {
              id: session.organizationDetails.userID,
              name: session.organizationDetails.userName
            }
          }
        }
      },
      "*"
    );
  };

  const getIsActive = (item) => {
    return !session.filterResetApiInProgress && item._id == currentActiveItemId;
  };

  return (
    <>
      <div className={Styles.postContainer}>
        <div className={Styles.itemIndicatorContainer}>
          {session.feed?.items[index]?.items?.length >
            (moveToNextSlide ? 0 : 1) &&
            session.feed?.items[index]?.items.map((e, index) => {
              return (
                <div className={Styles.baseProgressBar} key={index}>
                  <span
                    key={index}
                    ref={(node) => {
                      if (node) {
                        progressBarRefs.current[index] = node;
                      }
                    }}
                    className={
                      index <= activeItemIndex
                        ? Styles.itemIndicatorActive
                        : Styles.itemIndicatorInactive
                    }
                    style={{
                      backgroundColor:
                        (moveToNextSlide && index < activeItemIndex) ||
                        (!moveToNextSlide && index <= activeItemIndex)
                          ? "white"
                          : undefined
                    }}
                  ></span>
                </div>
              );
            })}
        </div>
        {session.filterItemsNotPresent ? (
          <div className={Styles.noPostsSection}>
            <img src={NoPosts} alt="noPosts" />
            <span>No Items Found</span>
            <span>Try using another filter.</span>
          </div>
        ) : (
          <>
            {session.feed?.items[index]?.items.length > 1 ? (
              <>
                <Carousel
                  swipeable={false}
                  autoPlay={false}
                  stopOnHover={false}
                  interval={600000}
                  axis="horizontal"
                  transitionTime={150}
                  selectedItem={activeItemIndex}
                  showStatus={false}
                  swipeScrollTolerance={5} // dont remove or reduce this as its important for video to play
                  showThumbs={false}
                  showArrows={false}
                  showIndicators={false}
                >
                  {session.feed?.items[index]?.items?.map((item, itemIndex) => {
                    const itemActiveState = getIsActive(item);
                    return item.type === "IMAGE" ? (
                      <div className={Styles.imageSection} key={itemIndex}>
                        <StoriesImage
                          updateProgressBarRef={(value) => {
                            try {
                              if (progressBarRefs?.current?.length) {
                                progressBarRefs.current[
                                  itemIndex
                                ].style.flexGrow = value / 100;
                                progressBarRefs.current[
                                  itemIndex
                                ].style.backgroundColor = "white";
                                progressBarRefs.current.forEach(
                                  (el, barIndex) => {
                                    if (barIndex < itemIndex) {
                                      el.style.flexGrow = 1;
                                    } else if (barIndex > itemIndex) {
                                      el.style.flexGrow = 0;
                                    }
                                  }
                                );
                              }
                            } catch (error) {
                              console.log(error);
                            }
                          }}
                          moveToNextSlide={moveToNextSlide}
                          item={item}
                          active={getIsActive(item)}
                          assetIndex={index}
                        />
                      </div>
                    ) : (
                      <div className={Styles.videoSection} key={itemIndex}>
                        {videoPlayer.isHlsLoaded && (
                          <StoriesVideoPlayer
                            updateProgressBarRef={(value) => {
                              try {
                                if (
                                  value !== 0 &&
                                  progressBarRefs?.current?.length
                                ) {
                                  progressBarRefs.current[
                                    itemIndex
                                  ].style.flexGrow = value / 100;
                                  progressBarRefs.current[
                                    itemIndex
                                  ].style.backgroundColor = "white";
                                }
                                progressBarRefs.current.forEach(
                                  (el, barIndex) => {
                                    if (barIndex < itemIndex) {
                                      el.style.flexGrow = 1;
                                    } else if (barIndex > itemIndex) {
                                      el.style.flexGrow = 0;
                                    }
                                  }
                                );
                              } catch (error) {
                                console.log(error);
                              }
                            }}
                            moveToNextSlide={moveToNextSlide}
                            initializePlayer={shouldInitializePlayer(index)}
                            active={itemActiveState}
                            currentActiveItemId={currentActiveItemId}
                            item={item}
                            assetIndex={index}
                            isMuted={videoPlayer.isMuted}
                            setIsMuted={videoPlayer.setIsMuted}
                          />
                        )}
                      </div>
                    );
                  })}
                </Carousel>
                <div
                  className={`${Styles.carouselArrow} ${Styles.prevBtn}`}
                  onClick={() => {
                    if (activeItemIndex) {
                      setActiveItemIndex(activeItemIndex - 1);
                      session.setCurrentActiveItemId(
                        sessionRef.current.feed.items[index].items[
                          activeItemIndex - 1
                        ]._id
                      );
                    } else if (
                      activeItemIndex === 0 &&
                      session.feed?.items[index - 1]?.items.length
                    ) {
                      moveToPreviousVerticalSlide();
                    }
                  }}
                ></div>
                <div
                  className={`${Styles.carouselArrow} ${Styles.nextBtn}`}
                  onClick={() => {
                    if (
                      activeItemIndex <
                      session.feed?.items[index]?.items.length - 1
                    ) {
                      setActiveItemIndex(activeItemIndex + 1);
                      session.setCurrentActiveItemId(
                        sessionRef.current.feed.items[index].items[
                          activeItemIndex + 1
                        ]._id
                      );
                    } else if (
                      activeItemIndex ===
                        session.feed?.items[index]?.items.length - 1 &&
                      session.feed?.items[index + 1]?.items.length
                    ) {
                      moveToNextSlide();
                    } else {
                      window.history.back();
                    }
                  }}
                ></div>
              </>
            ) : (
              session.feed?.items[index]?.items.map((item, ind) => {
                return (
                  <div key={ind} className={Styles.postSection}>
                    {item.type === "IMAGE" ? (
                      <div className={Styles.imageSection}>
                        <StoriesImage
                          updateProgressBarRef={(value) => {
                            try {
                              if (progressBarRefs?.current?.length) {
                                progressBarRefs.current[0].style.flexGrow =
                                  value / 100;
                                progressBarRefs.current[0].style.backgroundColor =
                                  "white";
                              }
                            } catch (error) {
                              console.log(error);
                            }
                          }}
                          moveToNextSlide={moveToNextSlide}
                          item={item}
                          active={getIsActive(item)}
                          assetIndex={index}
                        />
                      </div>
                    ) : (
                      <div className={Styles.videoSection}>
                        {videoPlayer.isHlsLoaded && (
                          <StoriesVideoPlayer
                            updateProgressBarRef={(value) => {
                              try {
                                if (
                                  value !== 0 &&
                                  progressBarRefs?.current?.length
                                ) {
                                  progressBarRefs.current[0].style.flexGrow =
                                    value / 100;
                                  progressBarRefs.current[0].style.backgroundColor =
                                    "white";
                                }
                              } catch (error) {
                                console.log(error);
                              }
                            }}
                            moveToNextSlide={moveToNextSlide}
                            initializePlayer={shouldInitializePlayer(index)}
                            active={getIsActive(item)}
                            item={item}
                            currentActiveItemId={currentActiveItemId}
                            assetIndex={index}
                            isMuted={videoPlayer.isMuted}
                            setIsMuted={videoPlayer.setIsMuted}
                          />
                        )}
                      </div>
                    )}
                    <div
                      className={`${Styles.carouselArrow} ${Styles.prevBtn}`}
                      onClick={() => {
                        if (
                          activeItemIndex === 0 &&
                          session.feed?.items[index - 1]?.items.length
                        ) {
                          moveToPreviousVerticalSlide();
                        }
                      }}
                    ></div>
                    <div
                      className={`${Styles.carouselArrow} ${Styles.nextBtn}`}
                      onClick={() => {
                        if (
                          activeItemIndex ===
                            session.feed?.items[index]?.items.length - 1 &&
                          session.feed?.items[index + 1]?.items.length
                        ) {
                          moveToNextSlide();
                        } else {
                          window.history.back();
                        }
                      }}
                    ></div>
                  </div>
                );
              })
            )}
            <div className={Styles.detailsContainer}>
              <div className={`${Styles.profileSection} ${Styles.showContent}`}>
                <div className={Styles.imageContainer} onClick={openProfile}>
                  {!session.feed?.items[index]?._createdBy?.photoKey ? (
                    <img src={ProfileImage} className={Styles.profileLogo} />
                  ) : (
                    <img
                      src={session.feed?.items[index]?._createdBy?.photoKey}
                      className={Styles.profileLogo}
                      onError={(e) => {
                        if (
                          decodeURI(e.target.src) !==
                          session.feed?.items[index]?._createdBy
                            ?.photoKeyFallback
                        ) {
                          e.target.onerror = null;
                          e.target.src =
                            session.feed?.items[
                              index
                            ]?._createdBy?.photoKeyFallback;
                        }
                      }}
                    />
                  )}
                </div>
                <span onClick={openProfile} className={Styles.profileName}>
                  {session.feed?.items[index]?._createdBy?.name}
                </span>
              </div>
            </div>
          </>
        )}
      </div>
    </>
  );
};

export default (props) => {
  return <FeedItemComponent {...props} />;
};
