import React, { useRef, useEffect } from "react";
import Styles from "./SwipeUpForMore.module.scss";
import Arrows from "./assets/images/arrows.svg";

function SwipeUpForMore(props) {
  let { session } = props;
  const showSwipeMoreSection = useRef();
  let swipeTimeoutID;

  useEffect(() => {
    if (!swipeTimeoutID) {
      swipeTimeoutID = setTimeout(() => {
        if (showSwipeMoreSection.current) {
          showSwipeMoreSection.current.style.display = "flex";
        }
      }, 30000);
      const scrollHandler = () => {
        showSwipeMoreSection.current.style.display = "none";
        clearTimeout(swipeTimeoutID);
        if (document.getElementById("scrollableDiv")) {
          document
            .getElementById("scrollableDiv")
            .removeEventListener("scroll", scrollHandler);
        }
      };
      if (document.getElementById("scrollableDiv")) {
        document
          .getElementById("scrollableDiv")
          .addEventListener("scroll", scrollHandler);
      }
    }

    return () => {
      if (swipeTimeoutID) {
        clearTimeout(swipeTimeoutID);
      }
    };
  }, []);

  let container = document.getElementById("scrollableDiv");
  let dimensions;
  if (container) {
    dimensions = container?.getBoundingClientRect()?.width;
  } else {
    dimensions = session?.containerFeedWidth;
  }

  return (
    <>
      <div
        ref={showSwipeMoreSection}
        className={Styles.animateScrollSection}
        style={{ width: dimensions + "px" }}
      >
        <div className={Styles.arrow}>
          <img src={Arrows} />
        </div>
        <span>Swipe up for more</span>
      </div>
    </>
  );
}

export default SwipeUpForMore;
