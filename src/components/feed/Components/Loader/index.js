import React from "react";
import Styles from "./Loader.module.scss";

export default ({ productsLoader, filterLoader }) => {
  return (
    <div
      className={`${Styles.loader} ${filterLoader && Styles.filterLoader}`}
      style={{
        background: productsLoader ? "transparent" : "white"
      }}
    >
      <div className={Styles.mainLoaderContainer}>
        <svg className={Styles.loaderSection} viewBox="22 22 44 44">
          <circle
            class=" MuiCircularProgress-circleIndeterminate"
            cx="44"
            cy="44"
            r="20.2"
            fill="none"
            stroke-width="3.6"
          ></circle>
        </svg>
      </div>
    </div>
  );
};
