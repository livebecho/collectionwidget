import React, { useEffect } from "react";
import Styles from "./Notification.module.scss";

export default (props) => {
  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    props.setNotificationObject(null);
  };

  if (!props.notificationObject) {
    return null;
  }

  useEffect(() => {
    if (!props.notificationObject) {
      return;
    }
    setTimeout(() => {
      props.setNotificationObject(null);
    }, 3000);
  }, []);

  return (
    <div style={{ fontSize: "16px" }}>
      <div className={Styles.mainSection}>
        <div
          className={`${Styles.notifDiv} ${
            props.notificationObject.type === "info"
              ? Styles.infoDiv
              : props.notificationObject.type === "error"
              ? Styles.errorDiv
              : Styles.successDiv
          }`}
        >
          <div className={Styles.notifIcon}>
            {props.notificationObject.type === "info" ||
            props.notificationObject.type === "error" ? (
              <svg focusable="false" viewBox="0 0 24 24" aria-hidden="true">
                <path d="M11 15h2v2h-2zm0-8h2v6h-2zm.99-5C6.47 2 2 6.48 2 12s4.47 10 9.99 10C17.52 22 22 17.52 22 12S17.52 2 11.99 2zM12 20c-4.42 0-8-3.58-8-8s3.58-8 8-8 8 3.58 8 8-3.58 8-8 8z"></path>
              </svg>
            ) : (
              <svg focusable="false" viewBox="0 0 24 24" aria-hidden="true">
                <path d="M11,9H13V7H11M12,20C7.59,20 4,16.41 4,12C4,7.59 7.59,4 12,4C16.41,4 20,7.59 20, 12C20,16.41 16.41,20 12,20M12,2A10,10 0 0,0 2,12A10,10 0 0,0 12,22A10,10 0 0,0 22,12A10, 10 0 0,0 12,2M11,17H13V11H11V17Z"></path>
              </svg>
            )}
          </div>
          <div className={Styles.notifMessage}>
            {props.notificationObject.message}
          </div>
          <div className={Styles.notifAction}>
            <button className={Styles.closeBtn} onClick={() => handleClose()}>
              <span>
                <svg focusable="false" viewBox="0 0 24 24" aria-hidden="true">
                  <path d="M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z"></path>
                </svg>
              </span>
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};
