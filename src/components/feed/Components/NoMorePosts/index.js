import React from "react";
import NoPosts from "./assets/images/noPosts.png";
import Styles from "./NoMorePosts.module.scss";

export default () => {
  return (
    <div className={Styles.noPostsSection}>
      <img src={NoPosts} alt="noPosts" />
      <span>No Items Found</span>
      <span>Try using another filter.</span>
    </div>
  );
};
