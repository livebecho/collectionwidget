const get_local_storage_status = () => {
  try {
    localStorage.setItem("test", "test");
    localStorage.removeItem("test");
  } catch (e) {
    return false;
  }
  return true;
};

export default get_local_storage_status;
