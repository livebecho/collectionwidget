import get_local_storage_status from "./localStorageCheck.service";

const addAssetIdToLocalStorage = (assetId, feedId, variantId) => {
  if (get_local_storage_status) {
    let lastSeenPostIds = JSON.parse(
      localStorage.getItem("LB_LAST_SEEN_POST_IDS")
    );
    if (!lastSeenPostIds) {
      localStorage.setItem(
        "LB_LAST_SEEN_POST_IDS",
        JSON.stringify({
          [feedId]: {
            [variantId]: [assetId]
          }
        })
      );
    } else if (lastSeenPostIds && !lastSeenPostIds[feedId]) {
      localStorage.setItem(
        "LB_LAST_SEEN_POST_IDS",
        JSON.stringify({
          ...lastSeenPostIds,
          [feedId]: {
            [variantId]: [assetId]
          }
        })
      );
    } else if (
      lastSeenPostIds &&
      lastSeenPostIds[feedId] &&
      !lastSeenPostIds[feedId][variantId]
    ) {
      localStorage.setItem(
        "LB_LAST_SEEN_POST_IDS",
        JSON.stringify({
          ...lastSeenPostIds,
          [feedId]: {
            ...lastSeenPostIds[feedId],
            [variantId]: [assetId]
          }
        })
      );
    } else if (
      lastSeenPostIds &&
      lastSeenPostIds[feedId] &&
      lastSeenPostIds[feedId][variantId] &&
      lastSeenPostIds[feedId][variantId].length <= 9
    ) {
      let index = lastSeenPostIds[feedId][variantId].indexOf(assetId);
      if (index === -1) {
        lastSeenPostIds[feedId][variantId] =
          lastSeenPostIds[feedId][variantId].concat(assetId);
        localStorage.setItem(
          "LB_LAST_SEEN_POST_IDS",
          JSON.stringify(lastSeenPostIds)
        );
      } else {
        lastSeenPostIds[feedId][variantId] = lastSeenPostIds[feedId][
          variantId
        ].filter((item) => item !== assetId);
        lastSeenPostIds[feedId][variantId] =
          lastSeenPostIds[feedId][variantId].concat(assetId);
        localStorage.setItem(
          "LB_LAST_SEEN_POST_IDS",
          JSON.stringify(lastSeenPostIds)
        );
      }
    } else {
      lastSeenPostIds[feedId][variantId].shift();
      let index = lastSeenPostIds[feedId][variantId].indexOf(assetId);
      if (index === -1) {
        lastSeenPostIds[feedId][variantId] =
          lastSeenPostIds[feedId][variantId].concat(assetId);
        localStorage.setItem(
          "LB_LAST_SEEN_POST_IDS",
          JSON.stringify(lastSeenPostIds)
        );
      } else {
        lastSeenPostIds[feedId][variantId] = lastSeenPostIds[feedId][
          variantId
        ].filter((item) => item !== assetId);
        lastSeenPostIds[feedId][variantId] =
          lastSeenPostIds[feedId][variantId].concat(assetId);
        localStorage.setItem(
          "LB_LAST_SEEN_POST_IDS",
          JSON.stringify(lastSeenPostIds)
        );
      }
    }
  }
};

export default addAssetIdToLocalStorage;
