import axios from "axios";

export const getPublicFeed = async (
  serverURL,
  feedId,
  organizationToken,
  pageNumber,
  itemsPerPage,
  globalLoader,
  filter,
  assetId,
  productID
) => {
  const response = await axios.get(
    `${serverURL}/api/v1/public-feed/${feedId}`,
    {
      headers: {
        "Content-Type": "application/json",
        "x-authorization": `bearer ${organizationToken}`,
        Accept: "application/json",
        "x-domain":
          window.location.href === "https://www.livebecho.com/bewakoof-feed"
            ? "https://www.bewakoof.com"
            : window.location.origin
      },
      params: {
        page: pageNumber,
        itemsPerPage: itemsPerPage,
        globalLoader: globalLoader,
        systemFilter: filter,
        assetId: assetId,
        contentSorting: feedId === "6086ebc851ff08457598357b",
        productID: productID
      }
    }
  );
  return response;
};

export const getCollection = async (
  serverURL,
  collectionID,
  organizationToken
) => {
  const response = await axios.get(
    `${serverURL}/api/v1/collection/${collectionID}`,
    {
      headers: {
        "Content-Type": "application/json",
        "x-authorization": `bearer ${organizationToken}`,
        Accept: "application/json",
        "x-domain":
          window.location.href === "https://www.livebecho.com/bewakoof-feed"
            ? "https://www.bewakoof.com"
            : window.location.origin
      }
    }
  );
  return response;
};

export const getFeedByProfile = async (
  serverURL,
  feedId,
  profileId,
  organizationToken,
  pageNumber,
  itemsPerPage,
  globalLoader,
  filter
) => {
  const response = await axios.get(
    `${serverURL}/api/v1/feed/${feedId}/profile/${profileId}`,
    {
      headers: {
        "Content-Type": "application/json",
        "x-authorization": `bearer ${organizationToken}`,
        "x-domain": window.location.origin
      },
      params: {
        page: pageNumber,
        itemsPerPage: itemsPerPage,
        globalLoader: globalLoader,
        filter: filter
      }
    }
  );

  return response;
};

export const getFeedByAsset = async (
  serverURL,
  assetId,
  organizationToken,
  globalLoader
) => {
  const response = await axios.get(`${serverURL}/api/v1/assets/${assetId}`, {
    headers: {
      "Content-Type": "application/json",
      "x-authorization": `bearer ${organizationToken}`,
      "x-domain": window.location.origin
    },
    params: {
      globalLoader: globalLoader
    }
  });

  return response;
};
