import React, { useContext, useEffect } from "react";
import StoriesProfile from "../../Templates/Stories/ProfilePage";
import uniqid from "uniqid";
import { StoriesContext } from "../../../../Context/Stories.context";

const ProfilePage = (props) => {
  let session = useContext(StoriesContext);

  useEffect(() => {
    if (session) {
      session.setCurrentSessionId(props.sessionID || uniqid());
    }
  }, [props.sessionID]);

  return (
    <>{session.currentSessionId ? <StoriesProfile {...props} /> : undefined}</>
  );
};

export default (props) => {
  return <ProfilePage {...props} />;
};
