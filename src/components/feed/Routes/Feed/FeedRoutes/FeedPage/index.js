import React, { useContext, useEffect } from "react";
import StoriesFeedPage from "../../Templates/Stories/FeedPage";
import uniqid from "uniqid";
import { StoriesContext } from "../../../../Context/Stories.context";

const FeedPage = (props) => {
  let session = useContext(StoriesContext);

  useEffect(() => {
    if (session) {
      session.setCurrentSessionId(props.sessionID || uniqid());
    }
  }, [props.sessionID]);

  return (
    <>
      {session.currentSessionId ? (
        <StoriesFeedPage {...props} configuration={props.configuration} />
      ) : undefined}
    </>
  );
};

export default (props) => {
  return <FeedPage {...props} />;
};
