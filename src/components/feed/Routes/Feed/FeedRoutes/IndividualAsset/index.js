import React, { useContext, useEffect } from "react";
import StoriesIndividualAsset from "../../Templates/Stories/IndividualAsset";
import uniqid from "uniqid";
import { StoriesContext } from "../../../../Context/Stories.context";

const IndividualAsset = (props) => {
  let session = useContext(StoriesContext);

  useEffect(() => {
    if (session) {
      session.setCurrentSessionId(props.sessionID || uniqid());
    }
  }, [props.sessionID]);

  return (
    <>
      {session.currentSessionId ? (
        <StoriesIndividualAsset {...props} />
      ) : undefined}
    </>
  );
};

export default (props) => {
  return <IndividualAsset {...props} />;
};
