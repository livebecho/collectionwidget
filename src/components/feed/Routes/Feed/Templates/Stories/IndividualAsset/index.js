import React, { useContext, useEffect, useState, useRef } from "react";
import FeedComponent from "../../../../../Components/Stories/Feed";
import { StoriesContext } from "../../../../../Context/Stories.context";
import SwipeUpForMore from "../../../../../Components/SwipeUpForMore";

const IndividualAssetPage = (props) => {
  const session = useContext(StoriesContext);
  const [showSwipe, setShowSwipe] = useState(undefined);
  const showSwipeRef = useRef(showSwipe);

  useEffect(() => {
    const updatedProps = {
      ...props,
      filter: props.configuration?.filter || props.filter
    };
    session.setStoriesProps(updatedProps);
  }, []);

  useEffect(() => {
    if (!session.storiesProps.organizationToken) {
      return;
    }
    session.getIndividualAsset();
  }, [session.storiesProps.organizationToken]);

  useEffect(() => {
    if (
      typeof showSwipeRef.current === "undefined" &&
      session.currentActiveItemId
    ) {
      setShowSwipe(true);
      showSwipeRef.current = true;
    } else if (showSwipeRef.current && session.currentActiveItemId) {
      setShowSwipe(false);
      showSwipeRef.current = false;
    }
  }, [session.currentActiveItemId]);

  return (
    <>
      {Object.keys(session.storiesProps).length > 0 ? (
        <FeedComponent {...props} fullView />
      ) : undefined}
      {showSwipe && <SwipeUpForMore session={session} />}
    </>
  );
};

export default (props) => {
  return <IndividualAssetPage {...props} />;
};
