import React, { useContext, useEffect } from "react";
import FeedComponent from "../../../../../Components/Stories/Feed";
import { StoriesContext } from "../../../../../Context/Stories.context";

const FeedPage = (props) => {
  const session = useContext(StoriesContext);

  useEffect(() => {
    const updatedProps = {
      ...props,
      filter: props.configuration?.filter || props.filter
    };
    session.setStoriesProps(updatedProps);
  }, []);

  useEffect(() => {
    if (!session.storiesProps.organizationToken) {
      return;
    }
    session.getFeed();
  }, [session.storiesProps.organizationToken]);

  return (
    <>
      {Object.keys(session.storiesProps).length > 0 ? (
        <FeedComponent {...props} />
      ) : undefined}
    </>
  );
};

export default (props) => {
  return <FeedPage {...props} />;
};
