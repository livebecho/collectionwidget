import React, { useState, useEffect, useContext, useRef } from "react";
import Styles from "./ProfilePage.module.scss";
import { StoriesContext } from "../../../../../Context/Stories.context";
import TruncateMarkup from "react-truncate-markup";
import ProfileImage from "./assets/images/DefaultProfileImage.png";
import FeedComponent from "../../../../../Components/Stories/Feed";
import SwipeUpForMore from "../../../../../Components/SwipeUpForMore";

const UserProfile = (props) => {
  const [animateUp, setAnimateUp] = useState(false);
  const session = useContext(StoriesContext);
  const [showDescription, setShowDescription] = useState(false);
  const [fullView, setFullView] = useState(false);
  const [scrollableDivWidth, setScrollableDivWidth] = useState();
  const [showSwipe, setShowSwipe] = useState(undefined);
  const showSwipeRef = useRef(showSwipe);

  useEffect(() => {
    if (
      typeof showSwipeRef.current === "undefined" &&
      session.currentActiveItemId
    ) {
      setShowSwipe(true);
      showSwipeRef.current = true;
    } else if (showSwipeRef.current && session.currentActiveItemId) {
      setShowSwipe(false);
      showSwipeRef.current = false;
    }
  }, [session.currentActiveItemId]);

  const currentUserId = useRef();
  currentUserId.current = session.organizationDetails.userID;

  const scrollRef = useRef();
  const scrollPosRef = useRef(0);
  const currentUserName = useRef();
  currentUserName.current = session.organizationDetails.userName;

  const profileSection = document.getElementById("profileSection");

  let dimensions;
  if (profileSection) {
    dimensions = profileSection?.getBoundingClientRect();
  }
  useEffect(() => {
    const updatedProps = {
      ...props,
      filter: props.configuration?.filter || props.filter
    };
    session.setStoriesProps(updatedProps);
  }, []);

  useEffect(() => {
    if (!session?.storiesProps?.organizationToken) {
      return;
    }
    session.getProfileFeed();
  }, [session?.storiesProps?.organizationToken]);

  const setOverflow = (value) => {
    if (value) {
      setAnimateUp(false);
    } else {
      setAnimateUp(true);
    }
  };

  const fullViewRef = useRef();
  fullViewRef.current = fullView;
  const handleScroll = (e) => {
    if (!scrollPosRef.current) {
      scrollPosRef.current = document
        .getElementById("profileSection")
        ?.getBoundingClientRect().top;

      return;
    }
    if (
      !fullViewRef.current &&
      scrollPosRef.current >
        document.getElementById("profileSection")?.getBoundingClientRect().top
    ) {
      setFullView(true);
      document.getElementById("scrollableDiv").scrollBy({
        top: document
          .getElementById("profileFeedSection")
          ?.getBoundingClientRect().top,
        left: 0,
        behavior: "smooth"
      });
      return;
    }
    if (
      fullViewRef.current &&
      document.getElementById("profileSection")?.getBoundingClientRect().top >
        scrollPosRef.current
    ) {
      setFullView(false);
    }
    scrollPosRef.current = document
      .getElementById("profileSection")
      ?.getBoundingClientRect().top;
  };

  const moveToTheTop = () => {
    document.getElementById("scrollableDiv").scrollBy({
      top: -document.getElementById("scrollableDiv").scrollTop,
      left: 0,
      behavior: "smooth"
    });
    setFullView(false);
  };

  useEffect(() => {
    if (document.getElementById("scrollableDiv")) {
      document
        .getElementById("scrollableDiv")
        .addEventListener("scroll", handleScroll);
    }
    return () => {
      if (document.getElementById("scrollableDiv")) {
        document
          .getElementById("scrollableDiv")
          .removeEventListener("scroll", handleScroll);
      }
    };
  }, []);

  return (
    <div
      ref={(div) => {
        if (div) {
          const width = (div.getBoundingClientRect().height * 9) / 16;
          setScrollableDivWidth(width);
        }
      }}
      className={Styles.container}
    >
      <div
        id="scrollableDiv"
        className={`${Styles.scrollableDiv} `}
        style={
          animateUp
            ? { width: scrollableDivWidth + "px", overflow: "hidden" }
            : { width: scrollableDivWidth + "px" }
        }
      >
        <div>
          {session?.profileMeta && (
            <div id="profileSection" className={`${Styles.profileSection}`}>
              <div className={Styles.profileImage}>
                {!session?.feed?.photoURL ? (
                  <img src={ProfileImage} />
                ) : (
                  <img
                    src={session?.feed?.photoURL}
                    onError={(e) => {
                      if (
                        decodeURI(e.target.src) !==
                        session?.feed?.photoURLFallback
                      ) {
                        e.target.onerror = null;
                        e.target.src = session?.feed?.photoURLFallback;
                      }
                    }}
                  />
                )}
              </div>
              <div className={Styles.profileHeading}>
                <h1>{session?.feed?.name}</h1>
              </div>
              <div className={Styles.profileMeta}>
                <div className={Styles.metaData}>
                  <span>{session?.profileMeta?.totalPosts}</span>
                  <span className={Styles.title}>Posts</span>
                </div>
              </div>
              {session?.feed?.description && (
                <div className={Styles.profileDescription}>
                  {showDescription ? (
                    <span>
                      {session?.feed?.description}

                      <span
                        className={Styles.readMore}
                        onClick={() => setShowDescription(false)}
                      >
                        &nbsp;less
                      </span>
                    </span>
                  ) : (
                    <TruncateMarkup
                      lines={3}
                      ellipsis={
                        <span
                          className={Styles.readMore}
                          onClick={() => setShowDescription(true)}
                        >
                          &nbsp;...more
                        </span>
                      }
                    >
                      <span>{session?.feed?.description}</span>
                    </TruncateMarkup>
                  )}
                </div>
              )}{" "}
            </div>
          )}
          <div
            ref={scrollRef}
            id="profileFeedSection"
            style={{
              height: scrollableDivWidth
                ? (scrollableDivWidth * 16) / 9 + "px"
                : "100%"
            }}
            className={`${Styles.feedSection}`}
          >
            {Object.keys(session.storiesProps).length > 0 ? (
              <FeedComponent
                {...props}
                fullView={fullView}
                setOverflow={setOverflow}
                moveToTheTop={moveToTheTop}
              />
            ) : undefined}
          </div>
        </div>
      </div>
      {showSwipe && <SwipeUpForMore session={session} />}
    </div>
  );
};
export default (props) => {
  return <UserProfile {...props} />;
};
