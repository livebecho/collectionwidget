import FeedPage from "./Feed/FeedRoutes/FeedPage";
import IndividualAsset from "./Feed/FeedRoutes/IndividualAsset";
import UserProfile from "./Feed/FeedRoutes/ProfilePage";

export default {
  FeedPage,
  UserProfile,
  IndividualAsset
};
