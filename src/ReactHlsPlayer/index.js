import React, { useEffect } from "react";

function ReactHlsPlayer({ hlsConfig, playerRef, src, autoPlay, ...props }) {
  useEffect(() => {
    let hls;

    function _initPlayer() {
      if (hls != null) {
        hls.destroy();
      }

      const newHls = new window.Hls({
        enableWorker: false,
        maxBufferLength: 4,
        ...hlsConfig
      });

      if (playerRef.current != null) {
        newHls.attachMedia(playerRef.current);
      }

      newHls.on(window.Hls.Events.MEDIA_ATTACHED, () => {
        newHls.loadSource(src);

        newHls.on(window.Hls.Events.MANIFEST_PARSED, () => {
          if (autoPlay) {
            playerRef.current
              .play()
              .catch(() =>
                console.log(
                  "Unable to autoplay prior to user interaction with the dom."
                )
              );
          }
        });
      });

      newHls.on(window.Hls.Events.ERROR, async function (event, data) {
        if (data.fatal) {
          switch (data.type) {
            case window.Hls.ErrorTypes.NETWORK_ERROR:
              newHls.startLoad();
              break;
            case window.Hls.ErrorTypes.MEDIA_ERROR:
              newHls.recoverMediaError();
              break;
            default:
              _initPlayer();
              break;
          }
        }
      });

      hls = newHls;
    }

    if (window.Hls.isSupported()) {
      _initPlayer();
    }

    return () => {
      if (hls != null) {
        hls.destroy();
      }
    };
  }, [autoPlay, hlsConfig, playerRef, src]);

  const isHLSupported = window.Hls.isSupported();

  console.log("isHLSupported", isHLSupported);

  // If Media Source is supported, use HLS.js to play video
  if (isHLSupported) {
    return <video ref={playerRef} playsInline {...props} />;
  }
  // Fallback to using a regular video player if HLS is supported by default in the user's browser
  return (
    <video
      ref={playerRef}
      src={src}
      playsInline
      autoPlay={autoPlay}
      {...props}
      onPlay={() => {
        props.onPlay();
      }}
    />
  );
}

export default ReactHlsPlayer;
